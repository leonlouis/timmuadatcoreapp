import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  Image,
  TouchableOpacity,
  View,
  Dimensions,
  AsyncStorage,
} from 'react-native';
import {Text} from 'react-native-paper';
import Button from '../../components/Button';
import {styles} from '../../core/dashboard/FilterPostDashboard/styles';
import Header from '../../components/Header';
import Selection from '../../components/Selection';
import {Container} from 'native-base';
import TypeAdvertisement from '../../components/TypeAdvertisement/TypeAdvertisement';
import Categories from '../../components/Categories/Categories';
import Slider from '@react-native-community/slider';
import ValueRange from '../../components/ValueRange/ValueRange';
import ListAreas from '../../components/ListAreas/ListAreas';
import ListCity from '../../components/ListCity/ListCity';
import ListProject from '../../components/ListProject/ListProject';
import ListDistricts from '../../components/ListDistricts/ListDistricts';
import ListWards from '../../components/ListWards/ListWards';
import ListStreets from '../../components/ListStreets/ListStreets';
import ListRoom from '../../components/ListRoom/ListRoom';
import ListDirection from '../../components/ListDirection/ListDirection';
import {useFocusEffect} from '@react-navigation/native';
export default function FilterPostDashboard({route, navigation}) {
  const {object} = route?.params;
  const [checkSubmit, setCheckSubmit] = useState(false);
  const [keyQsearch, setKeyQsearch] = useState(object.keySearch || '');
  const [price, setPrice] = useState('');
  const [area, setArea] = useState('');
  //loaitinrao
  const [popupListAdver, setPopupListAdver] = useState(false);
  const [objectAdver, setObjectAdver] = useState({
    name: object?.filterList?.adverOption?.name || 'Tất cả',
    value: object?.filterList?.adverOption?.value || '',
  });
  //loaibatdongsan
  const [popupCategories, setPopupCategories] = useState(false);
  const [objectCategories, setObjectCategories] = useState({
    name: object?.filterList?.categoriesOption?.name || 'Tất cả',
    value: object?.filterList?.categoriesOption?.id || '',
  });
  //khoanggia
  const [popupValueRange, setPopupValueRange] = useState(false);
  const [objectValueRange, setObjectValueRange] = useState(
    object?.filterList?.valueRangeOption || {
      name: 'Tất cả',
      id: '',
    },
  );
  //khuVuc
  const [popupAreas, setPopupAreas] = useState(false);
  const [objectAreas, setObjectAreas] = useState(
    object?.filterList?.areasOption || {
      name: 'Tất cả',
      id: '',
    },
  );
  //dientich
  const [popupCity, setPopupCity] = useState(false);
  const [objectCity, setObjectCity] = useState({
    name: object?.filterList?.cityOption?.name || 'Trên toàn quốc',
    id: object?.filterList?.cityOption?.id || '',
  });
  //duAn
  const [popupProject, setPopupProject] = useState(false);
  const [objectProject, setObjectProject] = useState({
    name: object?.filterList?.projectOption?.name || 'Tất cả',
    id: object?.filterList?.projectOption?.id || '',
  });
  //quan/huyen
  const [popupDistricts, setPopupDistricts] = useState(false);
  const [objectDistricts, setObjectDistricts] = useState({
    name: object?.filterList?.districtsOption?.name || 'Tất cả',
    id: object?.filterList?.districtsOption?.id || '',
  });
  //phuong/xa
  const [popupWards, setPopupWards] = useState(false);
  const [objectWards, setObjectWards] = useState({
    name: object?.filterList?.wardsOption?.name || 'Tất cả',
    id: object?.filterList?.wardsOption?.id || '',
  });
  //duong/pho
  const [popupStreets, setPopupStreets] = useState(false);
  const [objectStreets, setObjectStreets] = useState({
    name: object?.filterList?.streetsOption?.name || 'Tất cả',
    id: object?.filterList?.streetsOption?.id || '',
  });
  //soPhongNgu
  const [popupListRoom, setPopupListRoom] = useState(false);
  const [objectRoom, setObjectRoom] = useState({
    name: object?.filterList?.roomOption?.name || 'Tất cả',
    value: object?.filterList?.roomOption?.value || '',
  });
  //huongnha
  const [popupListDirection, setPopupListDirection] = useState(false);
  const [objectDirection, setObjectDirection] = useState({
    name: object?.filterList?.directionOption?.name || 'Tất cả',
    value: object?.filterList?.directionOption?.value || '',
  });

  const setCloseSelectRoom = item => {
    setObjectRoom(item);
    setPopupListRoom(false);
  };
  const setCloseSelectDirection = item => {
    setObjectDirection(item);
    setPopupListDirection(false);
  };

  const setCloseSelect = item => {
    setObjectAdver(item);
    setPopupListAdver(false);
  };
  const setCloseSelectCategories = item => {
    setObjectCategories(item);
    setPopupCategories(false);
  };
  const setCloseSelectValueRange = item => {
    setObjectValueRange(item);
    setPrice(item.amount_from);
    setPopupValueRange(false);
  };
  const setCloseSelectAreas = item => {
    setObjectAreas(item);
    setArea(item.amount_from);
    setPopupAreas(false);
  };
  const setCloseSelectCity = item => {
    setObjectCity(item);
    setPopupCity(false);
  };
  const setCloseSelectProject = item => {
    setObjectProject(item);
    setPopupProject(false);
  };
  const setCloseSelectDistricts = item => {
    setObjectDistricts(item);
    setPopupDistricts(false);
  };
  const setCloseSelectWards = item => {
    setObjectWards(item);
    setPopupWards(false);
  };
  const setCloseSelectStreets = item => {
    setObjectStreets(item);
    setPopupStreets(false);
  };
  const ClearFilter = () => {
    setObjectAdver({
      name: 'Tất cả',
      value: '',
    });
    setObjectCategories({
      name: 'Tất cả',
      id: '',
    });
    setObjectValueRange({
      name: 'Tất cả',
      id: '',
    });
    setObjectAreas({
      name: 'Tất cả',
      id: '',
    });
    setObjectCity({
      name: 'Trên toàn quốc',
      id: '',
    });
    setObjectProject({
      name: 'Tất cả',
      id: '',
    });
    setObjectDistricts({
      name: 'Tất cả',
      id: '',
    });
    setObjectWards({
      name: 'Tất cả',
      id: '',
    });
    setObjectStreets({
      name: 'Tất cả',
      id: '',
    });
    setObjectRoom({
      name: 'Tất cả',
      value: '',
    });
    setObjectDirection({
      name: 'Tất cả',
      value: '',
    });
    setPrice('');
    setArea('');
    setKeyQsearch('');
    setCheckSubmit(false);
  };
  useFocusEffect(
    React.useCallback(() => {
      setKeyQsearch(object.keySearch || '');
    }, [object]),
  );
  const onFilterPressed = () => {
    setCheckSubmit(true);
    if (objectAdver.value !== '' && objectCity.id !== '') {
      let cityOption = objectCity;
      let districtsOption = objectDistricts;
      let wardsOption = objectWards;
      let directionOption = objectDirection;
      let roomOption = objectRoom;
      let adverOption = objectAdver;
      let categoriesOption = objectCategories;
      let PriceOption = price;
      let areaOption = area;
      let projectOption = objectProject;
      let kSearch = keyQsearch;
      let valueRangeOption = objectValueRange;
      let areasOption = objectAreas;
      let streetsOption = objectStreets;
      navigation.navigate('Search', {
        dataFilter: {
          cityOption,
          districtsOption,
          wardsOption,
          directionOption,
          roomOption,
          adverOption,
          categoriesOption,
          PriceOption,
          areaOption,
          kSearch,
          projectOption,
          valueRangeOption,
          areasOption,
          streetsOption,
        },
      });
    }
  };

  return (
    <Container style={{flex: 1}}>
      <Header
        keyHeader="Lọc đăng tin"
        navigation={navigation}
        type={'Noback'}
        iconRight={'icon'}
      />
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <Selection size={16}>
            <TouchableOpacity
              onPress={() => ClearFilter()}
              style={styles.blockTitleQLTK}>
              <Text style={styles.titleQLTK}>Xóa bộ lọc</Text>
            </TouchableOpacity>
            <View>
              <TouchableOpacity
                onPress={() => setPopupListAdver(true)}
                style={[
                  objectAdver.value == 0 && checkSubmit
                    ? {marginTop: 18, borderBottomColor: '#FF3B30'}
                    : {marginTop: 28, borderBottomColor: '#E6E6E6'},
                  styles.itemQlTKTPShoWNoti,
                  {marginTop: 0},
                ]}>
                <View>
                  <Text style={styles.textLeftInfo}>Loại tin rao</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text style={styles.textLeftIcon}>
                    {objectAdver.name || 'Tất cả'}
                  </Text>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNext}
                  />
                </View>
              </TouchableOpacity>
              {objectAdver.value == 0 && checkSubmit ? (
                <Text style={styles.ErrorItem}>
                  Chọn Loại tin rao để hiển thị
                </Text>
              ) : (
                <View />
              )}
            </View>
            <TouchableOpacity
              onPress={() => setPopupCategories(true)}
              style={[
                objectAdver.value == 0 && checkSubmit
                  ? {marginTop: 18}
                  : {marginTop: 28},
                styles.itemQlTKTPLast,
              ]}>
              <View>
                <Text style={styles.textLeftInfo}>Loại bất động sản</Text>
              </View>
              <View style={styles.blockAllIcon}>
                <Text style={styles.textLeftIcon}>
                  {objectCategories.name || 'Tất cả'}
                </Text>
                <Image
                  source={require('../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setPopupCity(true)}
              style={[styles.itemQlTKTP]}>
              <View>
                <Text style={styles.textLeftInfo}>Tỉnh/Thành phố</Text>
              </View>
              <View style={styles.blockAllIcon}>
                <Text style={styles.textLeftIcon}>{objectCity.name}</Text>
                <Image
                  source={require('../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </View>
            </TouchableOpacity>
            <View>
              <TouchableOpacity
                onPress={() => {
                  setPopupValueRange(true);
                }}
                style={[
                  styles.itemQlTKTP,
                  {borderBottomWidth: 0, paddingBottom: 32},
                ]}>
                <View>
                  <Text style={styles.textLeftInfo}>Mức giá</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text style={[styles.textLeftIcon]}>
                    {objectValueRange.name}
                  </Text>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNext}
                  />
                </View>
              </TouchableOpacity>
              <Slider
                style={{width: '100%'}}
                minimumValue={parseInt(objectValueRange.amount_from || 0)}
                maximumValue={parseInt(objectValueRange.amount_to || 0)}
                minimumTrackTintColor="#78788033"
                maximumTrackTintColor="#004182"
                onValueChange={item => setPrice(item)}
              />
            </View>
            <View>
              <TouchableOpacity
                onPress={() => setPopupAreas(true)}
                style={[
                  styles.itemQlTKTP,
                  {borderBottomWidth: 0, paddingBottom: 32},
                ]}>
                <View>
                  <Text style={styles.textLeftInfo}>Diện tích</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text style={[styles.textLeftIcon]}>{objectAreas.name}</Text>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNext}
                  />
                </View>
              </TouchableOpacity>
              <Slider
                style={{width: '100%'}}
                minimumValue={parseInt(objectAreas.amount_from || 0)}
                maximumValue={parseInt(objectAreas.amount_to || 0)}
                minimumTrackTintColor="#78788033"
                maximumTrackTintColor="#004182"
                onValueChange={item => setArea(item)}
              />
            </View>
            <View>
              <TouchableOpacity
                onPress={() => {
                  setPopupProject(true);
                }}
                style={[
                  objectCity.id == 0 && checkSubmit
                    ? {marginTop: 18, borderBottomColor: '#FF3B30'}
                    : {marginTop: 28, borderBottomColor: '#E6E6E6'},
                  styles.itemQlTKTPShoWNoti,
                ]}>
                <View>
                  <Text style={styles.textLeftInfo}>Dự án</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text style={styles.textLeftIcon}>{objectProject.name}</Text>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNext}
                  />
                </View>
              </TouchableOpacity>
              {objectCity.id == 0 && checkSubmit ? (
                <Text style={styles.ErrorItem}>
                  Chọn Tỉnh/Thành để hiển thị
                </Text>
              ) : (
                <View />
              )}
            </View>
            <View>
              <TouchableOpacity
                onPress={() => {
                  setPopupDistricts(true);
                }}
                style={[
                  objectCity.id == 0 && checkSubmit
                    ? {marginTop: 18, borderBottomColor: '#FF3B30'}
                    : {marginTop: 28, borderBottomColor: '#E6E6E6'},
                  styles.itemQlTKTPShoWNoti,
                ]}>
                <View>
                  <Text style={styles.textLeftInfo}>Quận/Huyện</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text style={styles.textLeftIcon}>
                    {objectDistricts.name}
                  </Text>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNext}
                  />
                </View>
              </TouchableOpacity>
              {objectCity.id == 0 && checkSubmit ? (
                <Text style={styles.ErrorItem}>
                  Chọn Tỉnh/Thành để hiển thị
                </Text>
              ) : (
                <View />
              )}
            </View>
            <View>
              <TouchableOpacity
                onPress={() => {
                  setPopupWards(true);
                }}
                style={[
                  objectCity.id == 0 && checkSubmit
                    ? {marginTop: 18, borderBottomColor: '#FF3B30'}
                    : {marginTop: 28, borderBottomColor: '#E6E6E6'},
                  styles.itemQlTKTPShoWNoti,
                ]}>
                <View>
                  <Text style={styles.textLeftInfo}>Phường/Xã</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text style={styles.textLeftIcon}>{objectWards.name}</Text>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNext}
                  />
                </View>
              </TouchableOpacity>
              {objectCity.id == 0 && checkSubmit ? (
                <Text style={styles.ErrorItem}>
                  Chọn Tỉnh/Thành để hiển thị
                </Text>
              ) : (
                <View />
              )}
            </View>
            <View>
              <TouchableOpacity
                onPress={() => {
                  setPopupStreets(true);
                }}
                style={[
                  objectCity.id == 0 && checkSubmit
                    ? {marginTop: 18, borderBottomColor: '#FF3B30'}
                    : {marginTop: 28, borderBottomColor: '#E6E6E6'},
                  styles.itemQlTKTPShoWNoti,
                ]}>
                <View>
                  <Text style={styles.textLeftInfo}>Đường/Phố</Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Text style={styles.textLeftIcon}>{objectStreets.name}</Text>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNext}
                  />
                </View>
              </TouchableOpacity>
              {objectCity.id == 0 && checkSubmit ? (
                <Text style={styles.ErrorItem}>
                  Chọn Tỉnh/Thành để hiển thị
                </Text>
              ) : (
                <View />
              )}
            </View>
            <TouchableOpacity
              onPress={() => {
                setPopupListRoom(true);
              }}
              style={[
                objectCity.id == 0 && checkSubmit
                  ? {marginTop: 18}
                  : {marginTop: 28},
                styles.itemQlTKTPLast,
              ]}>
              <View>
                <Text style={styles.textLeftInfo}>Số phòng ngủ</Text>
              </View>
              <View style={styles.blockAllIcon}>
                <Text style={styles.textLeftIcon}>{objectRoom.name}</Text>
                <Image
                  source={require('../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setPopupListDirection(true);
              }}
              style={[styles.itemQlTKTP]}>
              <View>
                <Text style={styles.textLeftInfo}>Hướng nhà</Text>
              </View>
              <View style={styles.blockAllIcon}>
                <Text style={styles.textLeftIcon}>{objectDirection.name}</Text>
                <Image
                  source={require('../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </View>
            </TouchableOpacity>
          </Selection>
        </ScrollView>
        <Selection size={16}>
          <Button
            labelStyle={styles.buttonTitle}
            style={styles.buttonLogin}
            type={'blue'}
            mode="contained"
            onPress={() => {
              onFilterPressed();
            }}>
            Áp dụng
          </Button>
        </Selection>
      </SafeAreaView>
      <TypeAdvertisement
        visible={popupListAdver}
        data={objectAdver}
        onChange={item => setCloseSelect(item)}
        header={'Loại tin rao'}
      />
      <Categories
        visible={popupCategories}
        data={objectCategories}
        onChange={item => setCloseSelectCategories(item)}
        keyFilter={objectAdver.value}
        header={'Loại bất động sản'}
      />
      <ValueRange
        visible={popupValueRange}
        data={objectValueRange}
        onChange={item => setCloseSelectValueRange(item)}
        keyFilter={objectAdver.value}
        header={'Mức giá'}
      />
      <ListAreas
        visible={popupAreas}
        data={objectAreas}
        onChange={item => setCloseSelectAreas(item)}
        header={'Diện tích'}
      />
      <ListCity
        visible={popupCity}
        data={objectCity}
        onChange={item => setCloseSelectCity(item)}
        header={'Tỉnh/Thành phố'}
      />
      <ListProject
        visible={popupProject}
        data={objectProject}
        onChange={item => setCloseSelectProject(item)}
        keyFilterProject={objectCity.id}
        header={'Dự án'}
      />
      <ListDistricts
        visible={popupDistricts}
        data={objectDistricts}
        keyFilter={objectCity.id}
        onChange={item => setCloseSelectDistricts(item)}
        header={'Quận/Huyện'}
      />
      <ListWards
        visible={popupWards}
        data={objectWards}
        keyFilterCity={objectCity.id}
        keyFilterDisctrict={objectDistricts.id}
        onChange={item => setCloseSelectWards(item)}
        header={'Phường/Xã'}
      />
      <ListStreets
        visible={popupStreets}
        data={objectStreets}
        keyFilterCity={objectCity.id}
        keyFilterDisctrict={objectDistricts.id}
        onChange={item => setCloseSelectStreets(item)}
        header={'Đường/Phố'}
      />
      <ListRoom
        visible={popupListRoom}
        data={objectRoom}
        onChange={item => setCloseSelectRoom(item)}
        header={'Số phòng ngủ'}
      />
      <ListDirection
        visible={popupListDirection}
        data={objectDirection}
        onChange={item => setCloseSelectDirection(item)}
        header={'Hướng nhà'}
      />
    </Container>
  );
}
