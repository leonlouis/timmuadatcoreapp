import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  FlatList,
  AsyncStorage,
  Dimensions,
} from 'react-native';
import {styles} from '../../core/post/poststep_1/styles';
import {Body, Container, Text} from 'native-base';
const {width, height} = Dimensions.get('screen');
import Button from '../../components/Button';
import Header from '../../components/Header';
import StepIndicator from 'react-native-step-indicator';
import Selection from '../../components/Selection';
import MapView from 'react-native-maps';
import TypeAdvertisement from '../../components/TypeAdvertisement/TypeAdvertisement';
import PositionMaps from '../../components/PositionMaps/PositionMaps';
import TextInput from '../../components/TextInput';

export default function PostStep_1({navigation}) {
  const [currentPosition, setCurrentPosition] = useState(0);
  const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 1,
    currentStepStrokeWidth: 1,
    stepStrokeCurrentColor: '#004182',
    stepStrokeWidth: 1,
    stepStrokeFinishedColor: '#004182',
    stepStrokeUnFinishedColor: '#E6E6E6',
    separatorFinishedColor: '#004182',
    separatorUnFinishedColor: '#E6E6E6',
    stepIndicatorFinishedColor: '#004182',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#004182',
    stepIndicatorLabelFontSize: 14,
    currentStepIndicatorLabelFontSize: 14,
    stepIndicatorLabelCurrentColor: '#ffffff',
    stepIndicatorLabelCurrentBackground: '#004182',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#777777',
    labelColor: '#999999',
    labelSize: 14,
    currentStepLabelColor: '#fe7013',
  };
  const [popupListAdver, setPopupListAdver] = useState(false);
  const [popupPositionMaps, setPopupPositionMaps] = useState(false);
  const [objectAdver, setObjectAdver] = useState('');
  const setCloseSelect = item => {
    setObjectAdver(item);
    setPopupListAdver(false);
  };
  const [objectPositionMap, setObjectPositionMap] = useState('');
  const setCloseMaps = item => {
    setObjectPositionMap(item);
    setPopupPositionMaps(false);
  };

  const renderItem = ({item}) => (
    <TouchableOpacity>
      <Image
        source={require('../../assets/dashboard/Group2391.png')}
        style={styles.image}
      />
    </TouchableOpacity>
  );

  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Đăng tin rao" navigation={navigation} />
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <View style={[styles.blockPost, {width: width / 2}]}>
            <StepIndicator
              stepCount={3}
              customStyles={customStyles}
              currentPosition={currentPosition}
            />
          </View>
          {currentPosition === 0 ? (
            <Selection size={16}>
              <TouchableOpacity
                onPress={() => setPopupListAdver(true)}
                style={[styles.itemQlTKTP]}>
                <View>
                  <Text style={styles.titleText}>Chọn loại bất động sản*</Text>
                  <Text
                    style={[
                      objectAdver
                        ? styles.textLeftInfo
                        : styles.textLeftInfoNotcontent,
                    ]}>
                    {objectAdver.name}
                  </Text>
                </View>
                <View style={styles.blockAllIcon}>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={[
                      objectAdver
                        ? styles.vecterNext
                        : styles.vecterNextNotContent,
                    ]}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPopupListAdver(true)}
                style={[styles.itemQlTKTP]}>
                <View>
                  <Text style={styles.titleText}>Chọn Tỉnh/Thành phố*</Text>
                  <Text style={styles.textLeftInfoNotcontent} />
                </View>
                <View style={styles.blockAllIcon}>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNextNotContent}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPopupListAdver(true)}
                style={[styles.itemQlTKTP]}>
                <View>
                  <Text style={styles.titleText}>Chọn Quận/Huyện*</Text>
                  <Text style={styles.textLeftInfoNotcontent} />
                </View>
                <View style={styles.blockAllIcon}>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNextNotContent}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPopupListAdver(true)}
                style={[styles.itemQlTKTP]}>
                <View>
                  <Text style={styles.titleText}>Chọn Phường/Xã</Text>
                  <Text style={styles.textLeftInfoNotcontent} />
                </View>
                <View style={styles.blockAllIcon}>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNextNotContent}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPopupListAdver(true)}
                style={[styles.itemQlTKTP]}>
                <View>
                  <Text style={styles.titleText}>Chọn Đường/Phố</Text>
                  <Text style={styles.textLeftInfoNotcontent} />
                </View>
                <View style={styles.blockAllIcon}>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNextNotContent}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPopupListAdver(true)}
                style={[styles.itemQlTKTP]}>
                <View>
                  <Text style={styles.titleText}>Chọn Dự án</Text>
                  <Text style={styles.textLeftInfoNotcontent} />
                </View>
                <View style={styles.blockAllIcon}>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNextNotContent}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPopupListAdver(true)}
                style={[styles.itemQlTKTP]}>
                <View>
                  <Text style={styles.titleText}>Địa chỉ chi tiết*</Text>
                  <Text style={styles.textLeftInfoNotcontent} />
                </View>
                <View style={styles.blockAllIcon}>
                  <Image
                    source={require('../../assets/post/LocationIcon.png')}
                    style={styles.vecterNextNotContent}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setPopupPositionMaps(true)}
                style={[styles.itemQlTKTP]}>
                <View>
                  <Text style={styles.titleText}>Vị trí trên bản đồ</Text>
                  <Text style={styles.textLeftInfoNotcontent} />
                </View>
                <View style={styles.blockAllIcon}>
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.vecterNextNotContent}
                  />
                </View>
              </TouchableOpacity>
              <MapView
                style={styles.ggMap}
                initialRegion={{
                  latitude: 21.0277644,
                  longitude: 105.8341598,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
                }}
                showsUserLocation
                minZoomLevel={11}
              />
            </Selection>
          ) : (
            <Selection size={16}>
              <Text style={styles.textMtcb}>Mô tả cơ bản</Text>
              <View style={styles.itemQlTKMXH}>
                <View>
                  <TextInput
                    style={[styles.inputLogin]}
                    placeholder="Tiêu đề tin*"
                    placeholderTextColor="#999999"
                    returnKeyType="next"
                    // value={fullname.value}
                    // onChangeText={text => setFullname({value: text, error: ''})}
                    // error={!!fullname.error}
                    // errorText={fullname.error}
                    theme={{colors: {primary: '#999999', text: '#000000'}}}
                    autoCapitalize="none"
                    marginVerNone={0}
                  />
                  <Text style={styles.textTT}>0/99</Text>
                </View>
              </View>
              <View style={styles.itemQlTKMXH}>
                <View>
                  <TextInput
                    style={[
                      styles.inputLogin,
                      {
                        paddingRight: 15,
                        height: 60,
                      },
                    ]}
                    placeholder="Diện tích*"
                    placeholderTextColor="#999999"
                    returnKeyType="next"
                    // value={fullname.value}
                    // onChangeText={text => setFullname({value: text, error: ''})}
                    // error={!!fullname.error}
                    // errorText={fullname.error}
                    theme={{colors: {primary: '#999999', text: '#000000'}}}
                    autoCapitalize="none"
                    marginVerNone={0}
                  />
                  <Text style={styles.m2DT}>m2</Text>
                </View>
              </View>
              <View style={[styles.itemQlTKMXH, {flexDirection: 'row'}]}>
                <View>
                  <TextInput
                    style={[
                      styles.inputLogin,
                      {
                        height: 60,
                        marginTop: 16,
                        width: width / 2 - 26,
                        marginRight: 20,
                      },
                    ]}
                    placeholder="Giá"
                    placeholderTextColor="#999999"
                    returnKeyType="next"
                    // value={fullname.value}
                    // onChangeText={text => setFullname({value: text, error: ''})}
                    // error={!!fullname.error}
                    // errorText={fullname.error}
                    theme={{colors: {primary: '#999999', text: '#000000'}}}
                    autoCapitalize="none"
                    marginVerNone={0}
                  />
                </View>
                <View>
                  <TextInput
                    style={[
                      styles.inputLogin,
                      {
                        height: 60,
                        marginTop: 16,
                        width: width / 2 - 26,
                      },
                    ]}
                    placeholder="Đơn vị giá"
                    placeholderTextColor="#999999"
                    returnKeyType="next"
                    // value={fullname.value}
                    // onChangeText={text => setFullname({value: text, error: ''})}
                    // error={!!fullname.error}
                    // errorText={fullname.error}
                    theme={{colors: {primary: '#999999', text: '#000000'}}}
                    autoCapitalize="none"
                    marginVerNone={0}
                  />
                  <Image
                    source={require('../../assets/Vector.png')}
                    style={styles.downUp}
                  />
                </View>
              </View>
              <View style={styles.blockPriceImage}>
                <View style={styles.flex}>
                  <Text style={styles.totalPrice}>Tổng tiền:</Text>
                  <Text style={styles.price}>0</Text>
                </View>
                <View style={styles.flex}>
                  <Text style={[styles.totalPrice, {marginBottom: 28}]}>
                    Hình ảnh
                  </Text>
                  <Image
                    source={require('../../assets/post/bxs-info-circle.png')}
                    style={{marginLeft: 12}}
                  />
                </View>
                <View style={styles.blockImage}>
                  <FlatList
                    horizontal={true}
                    data={[{}, {}, {}, {}]}
                    renderItem={renderItem}
                  />
                  <TouchableOpacity style={styles.blockAddImage}>
                    <Image
                      source={require('../../assets/post/bx-image-add.png')}
                    />
                    <Text style={styles.imageAdd}>Thêm ảnh</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.itemQlTKMXH}>
                  <View>
                    <TextInput
                      style={[styles.inputLogin, {height: 60}]}
                      placeholder="Nội dung mô tả*"
                      placeholderTextColor="#999999"
                      returnKeyType="next"
                      // value={fullname.value}
                      // onChangeText={text => setFullname({value: text, error: ''})}
                      // error={!!fullname.error}
                      // errorText={fullname.error}
                      theme={{colors: {primary: '#999999', text: '#000000'}}}
                      autoCapitalize="none"
                      marginVerNone={0}
                    />
                    <Text style={styles.textTT}>0/3000</Text>
                  </View>
                </View>
                <Text style={styles.textMtcb}>Thông tin khác</Text>
                <View style={styles.blockInfo}>
                  <Text style={styles.textRightInfo}>Số tầng</Text>
                  <View style={styles.blockNumber}>
                    <View style={[styles.blockNumberTr, {marginRight: 12}]}>
                      <Text style={styles.numberTr}>-</Text>
                    </View>
                    <View>
                      <TextInput
                        style={[
                          styles.inputLogin,
                          {
                            height: 29,
                            width: 50,
                            borderWidth: 1,
                            borderColor: '#CECED0',
                            borderRadius: 4,
                          },
                        ]}
                        placeholder=""
                        placeholderTextColor="#999999"
                        returnKeyType="next"
                        // value={fullname.value}
                        // onChangeText={text => setFullname({value: text, error: ''})}
                        // error={!!fullname.error}
                        // errorText={fullname.error}
                        theme={{colors: {primary: '#999999', text: '#000000'}}}
                        autoCapitalize="none"
                        marginVerNone={0}
                      />
                    </View>
                    <View style={[styles.blockNumberTr, {marginLeft: 12}]}>
                      <Text style={styles.numberTr}>+</Text>
                    </View>
                  </View>
                </View>
              </View>
            </Selection>
          )}
        </SafeAreaView>
      </ScrollView>
      <Selection
        size={16}
        style={{borderTopWidth: 1, borderTopColor: '#E6E6E6'}}>
        <Button
          labelStyle={styles.buttonTitle}
          style={styles.buttonLogin}
          type="blue"
          mode="contained"
          onPress={() => {
            setCurrentPosition(currentPosition + 1);
          }}>
          Tiếp tục
        </Button>
      </Selection>
      <TypeAdvertisement
        visible={popupListAdver}
        data={objectAdver}
        onChange={item => setCloseSelect(item)}
        header={'Vị trí'}
      />
      <PositionMaps
        visible={popupPositionMaps}
        data={objectPositionMap}
        onChange={item => setCloseMaps(item)}
      />
    </Container>
  );
}
