import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  Dimensions,
} from 'react-native';
import styles from '../../core/Component/listcountry/styles';
import {Users} from '../../models/users';
import SanFrancisco from '../SanFrancisco';
import Const from '../Const';
import Selection from '../Selection';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';

const {width, height} = Dimensions.get('screen');

function PositionMaps({visible, onChange, data, ...props}) {
  //Map
  const ASPECT_RATIO = width / height;
  const LATITUDE_DELTA = 0.005; // Mức thu phóng rất cao
  const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
  const [listMarker, setListMarker] = useState([
    {
      latitude: 21.0277644,
      longitude: 105.8341598,
    },
  ]);
  const [addressCenter, setAddressCenter] = useState(0);
  const [addressCurrent, setAddressCurrent] = useState(0);
  const [center, setCenter] = useState({
    latitude: 21,
    longitude: 108,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  });
  const [region, setRegion] = useState({
    latitude: 21.0277644,
    longitude: 105.8341598,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  const [object, setObject] = useState(data || '');
  const [listPlaceData, setListPlaceData] = useState([]);
  const renderItem = ({item}) => (
    <TouchableOpacity
      style={{
        borderBottomWidth: 1,
        borderBottomColor: '#E6E6E6',
        paddingTop: 12,
        paddingBottom: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}
      onPress={() => setObject(item)}>
      <Text
        style={[
          {
            ...SanFrancisco.regular,
            fontSize: 17,
            lineHeight: 22,
            color: Const.BLACK_ROOT,
          },
          item.value === object.value
            ? {color: Const.BLUE}
            : {
                color: Const.BLACK_ROOT,
              },
        ]}>
        {item.name}
      </Text>
      {item.value === object.value ? (
        <Image source={require('../../assets/post/Symbol.png')} />
      ) : null}
    </TouchableOpacity>
  );
  useEffect(() => {
    if (!visible) {
      return;
    }
    setObject(data);
    setListPlaceData([
      {
        name: 'Tất cả',
        value: '1',
      },
      {
        name: 'Nhà đất bán',
        value: '2',
      },
      {
        name: 'Nhà đất cho thuê',
        value: '3',
      },
    ]);
  }, [visible]);

  const setWallet = () => {
    onChange(object);
  };

  if (!visible) {
    return <View />;
  }
  const handleToCenter = center => {
    setCenter(center);
    setRegion({...center});
    setAddressCurrent(addressCenter);
  };

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        zIndex: 9999999,
        position: 'absolute',
      }}>
      <View
        style={{
          backgroundColor: '#ffffff',
          height: '100%',
          width: '100%',
        }}>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            marginBottom: 16,
            flexDirection: 'row',
            justifyContent: 'space-between',
            height: 44,
            backgroundColor: '#F9F9F9F0',
            paddingTop: 12,
            paddingBottom: 12,
            paddingLeft: 16,
            paddingRight: 16,
          }}>
          <TouchableOpacity onPress={() => setWallet()}>
            <Image source={require('../../assets/back.png')} />
          </TouchableOpacity>
          <Text
            style={{
              ...SanFrancisco.semiBold,
              fontSize: 17,
              lineHeight: 20,
              color: Const.BLACK_ROOT,
            }}>
            Vị trí
          </Text>
          <TouchableOpacity onPress={() => setWallet()}>
            <Text
              style={{
                ...SanFrancisco.regular,
                fontSize: 17,
                lineHeight: 20,
                color: Const.BLUE,
              }}
            />
          </TouchableOpacity>
        </View>
        <MapView
          style={{flex: 1, height: height}}
          initialRegion={{
            latitude: 21.0277644,
            longitude: 105.8341598,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          onRegionChangeComplete={coordinate => {
            console.log('coordinate', coordinate);
          }}
          onPress={e => {
            console.log('e.nativeEvent.coordinate', e.nativeEvent.coordinate);
            setListMarker([...listMarker, e.nativeEvent.coordinate]);
          }}
          region={region}
          showsUserLocation
          provider={PROVIDER_GOOGLE}>
          {listMarker.map(marker => (
            <Marker coordinate={marker} />
          ))}
        </MapView>
        {/*<TouchableOpacity*/}
        {/*  style={{*/}
        {/*    position: 'absolute',*/}
        {/*    top: 200,*/}
        {/*    right: 20,*/}
        {/*    zIndex: 99999,*/}
        {/*  }}*/}
        {/*  activeOpacity={0.9}*/}
        {/*  onPress={() => handleToCenter(center)}>*/}
        {/*  <Text>Zoom</Text>*/}
        {/*</TouchableOpacity>*/}
      </View>
    </View>
  );
}

export default PositionMaps;
