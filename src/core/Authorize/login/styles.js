import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  inputLogin: {
    height: 44,
    fontSize: 17,
    lineHeight: 22,
    ...SanFrancisco.regular,
    color: '#000000',
  },
  buttonTitle: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 28,
    color: '#FFFFFF',
  },
  buttonLogin: {
    borderRadius: 8,
    height: 50,
  },
  blockNagation: {
    marginTop: 19,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textAuthen: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 20,
    color: '#429134',
  },
  createAcount: {
    marginTop: 26,
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 20,
    color: '#999999',
  },
  inputeye: {
    position: 'absolute',
    right: 0,
    top: 15,
  },
  IconMxh: {
    marginTop: 29,
    flexDirection: 'row',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pdlr32: {
    marginRight: 32,
    marginLeft: 32,
  },
  textHow: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK,
    textAlign: 'center',
  },
  textRegister: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLUE,
    textAlign: 'center',
  },
  blockTextHow: {
    flexDirection: 'row',
    marginTop: 60,
    justifyContent: 'center',
  },
};
