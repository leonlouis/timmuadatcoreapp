import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  View,
  FlatList,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  AsyncStorage,
} from 'react-native';
import {styles} from '../../core/dashboard/styles';
import {MainBottomTabComponent} from '../../components/BottomTab';
import {Container} from 'native-base';
import TextInput from '../../components/TextInput';
import Selection from '../../components/Selection';
import Animated from 'react-native-reanimated';

const {width, height} = Dimensions.get('screen');
import {SliderBox} from 'react-native-image-slider-box';
import {TabView, SceneMap} from 'react-native-tab-view';
import TabPostForyou from './TabPostForyou/TabPostForyou';
import {useFocusEffect} from '@react-navigation/native';
import {DashboardModel} from '../../models/dashboard';
import TabRentalNews from './TabRentalNews/TabRentalNews';
import TabForSale from './TabForSale/TabForSale';

export default function Dashboard({route, navigation}, ...props) {
  const {object} = route?.params;
  const [objectFilter, setObjectFilter] = useState(object || '');
  const [listData, setListData] = useState('');
  const [listDataForYou, setListDataForYou] = useState([]);
  const [keySearchDasboard, setKeySearchDasboard] = useState({value: ''});
  const [slider, setSlider] = useState([]);
  const _handleIndexChange = i => setIndex(i);

  const [index, setIndex] = React.useState(0);
  const renderScene = ({route}) => {
    switch (route.key) {
      case '1':
        return <TabPostForyou navigation={navigation} data={listDataForYou} />;
      case '2':
        return (
          <TabForSale
            navigation={navigation}
            data={listData.listProductTypeSell}
          />
        );
      case '3':
        return (
          <TabRentalNews
            navigation={navigation}
            data={listData.listProductTypeBuy}
          />
        );
      default:
        return null;
    }
  };
  const [routes] = React.useState([
    {key: '1', title: 'Dành cho bạn'},
    {key: '2', title: 'Tin rao bán'},
    {key: '3', title: 'Tin rao cho thuê'},
  ]);
  const _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View style={[styles.tabBar]}>
        {props.navigationState.routes.map((route, i) => {
          return (
            <TouchableOpacity
              style={[
                styles.tabItem,
                index === i ? {backgroundColor: '#004182'} : {},
              ]}
              onPress={() => setIndex(i)}>
              <Animated.Text
                style={[
                  styles.titleTab,
                  index === i ? {color: '#ffffff'} : {color: '#000000'},
                ]}>
                {route.title}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };
  const getListData = () => {
    DashboardModel.indexAction()
      .then(data => {
        if (data.status == 1) {
          setListData(data.data);
          let Ar = [];
          if (
            data?.data?.listSlides !== null ||
            data?.data?.listSlides.length > 0
          ) {
            data?.data?.listSlides.forEach(item => {
              Ar.push(item.avatar);
            });
          }
          setSlider(Ar);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
  const getDataInterestProducts = () => {
    DashboardModel.getListInterestProducts(
      objectFilter?.filterList?.cityOption?.id,
      objectFilter?.filterList?.districtsOption?.id,
      objectFilter?.filterList?.wardsOption?.id,
      objectFilter?.filterList?.directionOption?.value,
      objectFilter?.filterList?.roomOption?.value,
      objectFilter?.filterList?.adverOption?.value,
      objectFilter?.filterList?.categoriesOption?.id,
      objectFilter?.filterList?.PriceOption,
      objectFilter?.filterList?.areaOption,
      objectFilter?.filterList?.projectOption?.id,
      objectFilter?.keySearch,
      '',
      '',
    )
      .then(data => {
        if (data.status == 1) {
          setListDataForYou(data.data.listProducts);
        }
      })
      .catch(e => {
        console.log('e', e);
      });
  };
  useFocusEffect(
    React.useCallback(() => {
      setObjectFilter(object || '');
      getListData();
      getDataInterestProducts();
    }, []),
  );
  return (
    <Container style={{flex: 1}}>
      <SafeAreaView style={styles.container}>
        <Selection size={16}>
          <View style={styles.blockSearch}>
            <View>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Search', {
                    dataFilter: {
                      kSearch: keySearchDasboard.value,
                    },
                  });
                }}
                style={styles.iconInptuSearch}>
                <Image
                  source={require('../../assets/dashboard/SearchGlyphLight.png')}
                />
              </TouchableOpacity>
              <TextInput
                style={[
                  styles.inputLogin,
                  {
                    textAlign: 'left',
                    width: width - 86,
                  },
                ]}
                underlineColor="transparent"
                placeholder="Nhập từ khóa tìm kiếm..."
                placeholderTextColor="#999999"
                returnKeyType="next"
                // value={email.value}
                onChangeText={text =>
                  setKeySearchDasboard({value: text, error: ''})
                }
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                autoCapitalize="none"
                autoCompleteType="email"
                textContentType="emailAddress"
                keyboardType="email-address"
                marginVerNone={0}
              />
            </View>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('FilterPostDashboard', {
                  object: {keySearch: keySearchDasboard.value},
                });
              }}
              style={styles.iconFind}>
              <Image source={require('../../assets/dashboard/Group1755.png')} />
            </TouchableOpacity>
          </View>
        </Selection>
        <View>
          <SliderBox
            images={slider}
            paginationBoxStyle={{
              position: 'absolute',
              bottom: 0,
              left: 10,
              padding: 0,
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
            }}
            ImageComponentStyle={{
              borderRadius: 10,
              width: width - 36,
            }}
            resizeMethod={'resize'}
            resizeMode={'cover'}
            sliderBoxHeight={155}
          />
        </View>
        <TabView
          navigationState={{index, routes}}
          renderScene={renderScene}
          renderTabBar={_renderTabBar}
          onIndexChange={_handleIndexChange}
        />
      </SafeAreaView>
      <MainBottomTabComponent navigation={navigation} activeTab="home" />
    </Container>
  );
}
