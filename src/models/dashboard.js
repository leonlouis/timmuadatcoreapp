import {config} from '../config/config';

export const DashboardModel = {
  indexAction: () => {
    return fetch(config.endpointUrl + 'api/dashboard/index', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  filterDataAction: () => {
    return fetch(config.endpointUrl + 'api/dashboard/filterData', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
  getPriceConditions: filter => {
    return fetch(
      config.endpointUrl + 'api/dashboard/priceConditions/' + filter,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: global.token,
        },
      },
    ).then(data => data.json());
  },
  getListInterestProducts: (
    province_id,
    district_id,
    ward_id,
    home_direction,
    bedroom_numbers,
    category_type,
    categories_id,
    price_id,
    area_id,
    project_id,
    keyword,
    sort,
    limit,
  ) => {
    let param = '/api/dashboard/listInterestProducts';
    if (province_id !== '') {
      param += `?province_id=${province_id}`;
    }
    if (district_id !== '') {
      param += `&district_id=${district_id}`;
    }
    if (ward_id !== '') {
      param += `&ward_id=${ward_id}`;
    }
    if (home_direction !== '') {
      param += `&home_direction=${home_direction}`;
    }
    if (bedroom_numbers !== '') {
      param += `&bedroom_numbers=${bedroom_numbers}`;
    }
    if (category_type !== '') {
      param += `&category_type=${category_type}`;
    }
    if (categories_id !== '') {
      param += `&categories_id=${categories_id}`;
    }
    if (price_id !== '') {
      param += `&price_id=${price_id}`;
    }
    if (area_id !== '') {
      param += `&area_id=${area_id}`;
    }
    if (project_id !== '') {
      param += `&project_id=${project_id}`;
    }
    if (keyword !== '') {
      param += `&keyword=${keyword}`;
    }
    if (sort !== '') {
      param += `&sort=${sort}`;
    }
    if (limit !== '') {
      param += `&limit=${limit}`;
    }
    console.log('zxczzz', config.endpointUrl + param);
    return fetch(config.endpointUrl + param, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
};
