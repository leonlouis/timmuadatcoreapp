import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  buttonTitle: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 22,
    color: Const.WHITE,
  },
  buttonLogin: {
    borderRadius: 8,
    height: 50,
    borderWidth: 1,
    borderColor: Const.BLUE,
    // marginTop: 13,
    marginBottom: 16,
    marginTop: 16,
  },
  //issetInfo
  blockTitle: {
    marginTop: 16,
  },
  infoUser: {
    alignItems: 'center',
    marginBottom: 23,
  },
  username_info: {
    ...SanFrancisco.semiBold,
    fontSize: 19,
    lineHeight: 23,
    color: Const.BLACK,
  },
  level_info: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 20,
    color: Const.ORANGE,
  },
  value_money: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 20,
    color: Const.GRAY_v4,
    paddingTop: 4,
    paddingBottom: 4,
  },
  info_full: {
    marginLeft: 16,
  },
  blockTitleQLTK: {
    paddingTop: 13,
    paddingBottom: 23,
    borderTopWidth: 1,
    borderTopColor: '#E6E6E6',
  },
  titleQLTK: {
    ...SanFrancisco.regular,
    fontSize: 16,
    lineHeight: 20,
    color: Const.BLACK,
    textTransform: 'uppercase',
  },

  itemQlTKMXH: {
    flexShrink: 1,
  },
  itemQlTKTP: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#E6E6E6',
    paddingTop: 20,
  },
  textLeftInfo: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 20,
    color: Const.GRAY_v5,
  },
  textRightInfo: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK,
    flexShrink: 1,
  },
  textRightInfoNodata: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.GRAY_v5,
    flexShrink: 1,
  },
  //
  blockIconUpdate: {
    backgroundColor: '#00000080',
    borderRadius: 50,
    paddingTop: 7,
    paddingBottom: 7,
    paddingRight: 6,
    paddingLeft: 6,
    position: 'absolute',
    bottom: -8,
    right: '40%',
  },
  inputLogin: {
    height: 60,
    fontSize: 17,
    lineHeight: 20,
    ...SanFrancisco.regular,
    color: '#000000',
    textAlign: 'left',
  },
  vecterNext: {
    marginTop: 8,
  },
  clearPhone: {
    position: 'absolute',
    right: 0,
    bottom: 10,
  },
  blockAddPhone: {
    flexDirection: 'row',
    marginLeft: 9,
    marginBottom: 9,
    marginTop: 9,
  },
  textAddPhone: {
    fontSize: 22,
    lineHeight: 26,
    ...SanFrancisco.regular,
    color: Const.BLUE,
    marginLeft: 9,
  },
  itemQlTK: {
    paddingBottom: 10,
    flexShrink: 1,
  },
};
