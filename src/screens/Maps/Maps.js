import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
} from 'react-native';
import {styles} from '../../core/maps/styles';
import {MainBottomTabComponent} from '../../components/BottomTab';
import {Body, Container, Text} from 'native-base';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';

const {width, height} = Dimensions.get('screen');

export default function Maps({navigation}) {
  const ASPECT_RATIO = width / height;
  const LATITUDE_DELTA = 0.005; // Mức thu phóng rất cao
  const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
  const [listMarker, setListMarker] = useState([
    {
      latitude: 21.0277644,
      longitude: 105.8341598,
    },
  ]);
  const [addressCenter, setAddressCenter] = useState(0);
  const [addressCurrent, setAddressCurrent] = useState(0);
  const [center, setCenter] = useState({
    latitude: 21,
    longitude: 108,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  });
  const [region, setRegion] = useState({
    latitude: 21.0277644,
    longitude: 105.8341598,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  const handleToCenter = center => {
    setCenter(center);
    setRegion({...center});
    setAddressCurrent(addressCenter);
  };
  return (
    <Container style={{flex: 1}}>
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <MapView
            style={{flex: 1, height: height}}
            initialRegion={{
              latitude: 21.0277644,
              longitude: 105.8341598,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            onRegionChangeComplete={coordinate => {
              console.log('coordinate', coordinate);
            }}
            onPress={e => {
              console.log('e.nativeEvent.coordinate', e.nativeEvent.coordinate);
              setListMarker([e.nativeEvent.coordinate]);
            }}
            region={region}
            showsUserLocation
            provider={PROVIDER_GOOGLE}>
            {listMarker.map(marker => (
              <Marker coordinate={marker} />
            ))}
          </MapView>
          {/*<TouchableOpacity*/}
          {/*  style={{*/}
          {/*    position: 'absolute',*/}
          {/*    top: 200,*/}
          {/*    right: 20,*/}
          {/*    zIndex: 99999,*/}
          {/*  }}*/}
          {/*  activeOpacity={0.9}*/}
          {/*  onPress={() => handleToCenter(center)}>*/}
          {/*  <Text>Zoom</Text>*/}
          {/*</TouchableOpacity>*/}
        </SafeAreaView>
      </ScrollView>
      <MainBottomTabComponent navigation={navigation} activeTab="maps" />
    </Container>
  );
}
