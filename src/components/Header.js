import React, {useState, useEffect} from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import {Text} from 'react-native-paper';
import SanFrancisco from './SanFrancisco';
import Const from './Const';

export default function Header(props) {
  //type  : main => Drawer Sidebar , back => show buttonBack , notback => hidden Back

  return (
    <View
      style={[
        styles.wrapHeader,
        props.background ? {backgroundColor: props.background} : '',
      ]}>
      <View style={styles.titleHeaderCenter}>
        <View style={styles.blockContentHeader}>
          {props.type !== 'Noback' ? (
            <TouchableOpacity
              style={styles.iconBack}
              onPress={() => props.navigation.goBack()}>
              <Image source={require('../assets/back.png')} />
            </TouchableOpacity>
          ) : (
            <View />
          )}
          <Text style={styles.titleHeader}>{props.keyHeader}</Text>
          {props.iconRight === 'text' ? (
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate(props.nameNavigation);
              }}>
              <Text style={styles.textRight}>Sửa</Text>
            </TouchableOpacity>
          ) : props.iconRight === 'icon' ? (
            <TouchableOpacity
              style={styles.iconBack}
              onPress={() => props.navigation.goBack()}>
              <Image
                source={require('../assets/Acount/filterpost/close.png')}
                style={{width: 17, height: 17}}
              />
            </TouchableOpacity>
          ) : (
            <Text />
          )}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  headerIcon: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  menu: {
    width: 22,
    height: 15,
  },
  multipleEng: {
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 21,
    letterSpacing: -0.3,
    color: '#FFFFFF',
    marginRight: 18,
  },
  iconBack: {},
  wrapHeader: {
    backgroundColor: '#F9F9F9F0',
    paddingTop: 16,
    paddingLeft: 16,
    paddingRight: 16,
  },
  titleHeaderCenter: {
    justifyContent: 'center',
    height: 44,
  },
  titleHeader: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 20,
    color: '#000000',
  },
  blockContentHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textRight: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 20,
    color: Const.BLUE,
  },
});
