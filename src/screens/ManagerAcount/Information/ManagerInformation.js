import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
  Alert,
  AsyncStorage,
} from 'react-native';
import {Text} from 'react-native-paper';
import {styles} from '../../../core/Acount/managerinformation/styles';
import {MainBottomTabComponent} from '../../../components/BottomTab';
import Selection from '../../../components/Selection';
import {Container} from 'native-base';
import FormatNumber from '../../../components/FormatNumber';

export default function ManagerInformation({navigation}) {
  return (
    <Container style={{flex: 1}}>
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <Selection size={16} style={styles.blockTitle}>
            <View style={styles.infoUser}>
              {global?.userinfo?.avatar ? (
                <Image
                  source={{
                    uri: global?.userinfo?.avatar,
                  }}
                  style={{
                    width: 74,
                    height: 74,
                    borderRadius: 50,
                  }}
                />
              ) : (
                <Image
                  source={require('../../../assets/ic_authorize/icon_login/MaskGroup309.png')}
                />
              )}
              <View style={styles.info_full}>
                <Text style={styles.username_info}>
                  {global?.userinfo?.username}
                </Text>
                <Text style={styles.level_info}>Tài khoản VIP</Text>
                <Text style={styles.value_money}>
                  Số dư:{' '}
                  <FormatNumber
                    value={global?.userinfo?.balance}
                    currency="đ"
                    style={styles.vlue}
                  />
                </Text>
              </View>
            </View>
          </Selection>
          <View>
            <View style={styles.blockTitleQLTK}>
              <Text style={styles.titleQLTK}>Quản lý tài khoản</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Information');
              }}>
              <Selection size={16} style={styles.itemQlTK}>
                <Text>Thông tin tài khoản</Text>
                <Image
                  source={require('../../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </Selection>
            </TouchableOpacity>
            <View style={styles.borderItem} />
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('ChangePassword');
              }}>
              <Selection size={16} style={styles.itemQlTK}>
                <Text>Đổi mật khẩu</Text>
                <Image
                  source={require('../../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </Selection>
            </TouchableOpacity>
          </View>
          <View>
            <View style={styles.blockTitleQLTK}>
              <Text style={styles.titleQLTK}>Quản lý tin rao</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('ListPost');
              }}>
              <Selection size={16} style={styles.itemQlTK}>
                <Text>Quản lý đăng tin</Text>
                <Image
                  source={require('../../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </Selection>
            </TouchableOpacity>
          </View>
          <View>
            <View style={styles.blockTitleQLTK}>
              <Text style={styles.titleQLTK}>Quản lý tài chính</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('InformationBalance');
              }}>
              <Selection size={16} style={styles.itemQlTK}>
                <Text>Thông tin số dư</Text>
                <Image
                  source={require('../../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </Selection>
            </TouchableOpacity>
            <View style={styles.borderItem} />
            <Selection size={16} style={styles.itemQlTK}>
              <Text>Lịch sử giao dịch</Text>
              <Image
                source={require('../../../assets/Vector.png')}
                style={styles.vecterNext}
              />
            </Selection>
            <View style={styles.borderItem} />
            <Selection size={16} style={styles.itemQlTK}>
              <Text>Khuyến mãi</Text>
              <Image
                source={require('../../../assets/Vector.png')}
                style={styles.vecterNext}
              />
            </Selection>
            <View style={styles.borderItem} />
            <Selection size={16} style={styles.itemQlTK}>
              <Text>Nạp tiền</Text>
              <Image
                source={require('../../../assets/Vector.png')}
                style={styles.vecterNext}
              />
            </Selection>
          </View>
          <View style={{marginBottom: 40}}>
            <View style={styles.blockTitleQLTK}>
              <Text style={styles.titleQLTK}>Tiện ích</Text>
            </View>
            <Selection size={16} style={styles.itemQlTK}>
              <Text>Thông báo</Text>
              <View style={styles.totalNotifi}>
                <View style={styles.blockNumberNotifi}>
                  <Text style={styles.numberNotifi}>23</Text>
                </View>
                <Image
                  source={require('../../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </View>
            </Selection>
            <View style={styles.borderItem} />
          </View>
        </ScrollView>
      </SafeAreaView>
      <MainBottomTabComponent navigation={navigation} activeTab="login" />
    </Container>
  );
}
