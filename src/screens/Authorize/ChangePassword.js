import React, {useState} from 'react';
import {Image, SafeAreaView, TouchableOpacity, View} from 'react-native';
import {Text} from 'react-native-paper';
import Button from '../../components/Button';
import {styles} from '../../core/Authorize/changepassword/styles';
import Header from '../../components/Header';
import Selection from '../../components/Selection';
import {Container} from 'native-base';
import TextInput from '../../components/TextInput';
import OtpInputs from 'react-native-otp-inputs';
import ModalProcess from '../../components/ListModal/ModalProcess';

export default function ChangePassword({navigation}) {
  const [otp, setOtp] = useState('');
  const [modalProcess, setModalProcess] = useState(false);
  const setCloseModal = item => {
    setModalProcess(false);
  };
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Đổi mật khẩu" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <ModalProcess
          visible={modalProcess}
          title={'Đổi mật khẩu thành công!'}
          content={'Bạn sẽ nhập mật khẩu mới cho lần đăng nhập tiếp theo.'}
          typeButton={1}
          onChange={item => setCloseModal(item)}
        />
        <Selection size={16}>
          <View>
            <TextInput
              style={styles.inputLogin}
              placeholder="Mật khẩu cũ*"
              placeholderTextColor="#999999"
              returnKeyType="next"
              // value={fullname.value}
              // onChangeText={text => setFullname({value: text, error: ''})}
              // error={!!fullname.error}
              // errorText={fullname.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
            />
          </View>
          <View>
            <TextInput
              style={styles.inputLogin}
              placeholder="Mật khẩu mới*"
              placeholderTextColor="#999999"
              returnKeyType="next"
              // value={fullname.value}
              // onChangeText={text => setFullname({value: text, error: ''})}
              // error={!!fullname.error}
              // errorText={fullname.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
            />
            <TouchableOpacity style={styles.inputeye}>
              <Image
                source={require('../../assets/ic_authorize/icon_login/bx-show.png')}
              />
            </TouchableOpacity>
          </View>
          <View>
            <TextInput
              style={styles.inputLogin}
              placeholder="Nhập lại mật khẩu mới*"
              placeholderTextColor="#999999"
              returnKeyType="next"
              // value={fullname.value}
              // onChangeText={text => setFullname({value: text, error: ''})}
              // error={!!fullname.error}
              // errorText={fullname.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
            />
            <TouchableOpacity style={styles.inputeye}>
              <Image
                source={require('../../assets/ic_authorize/icon_login/bx-show.png')}
              />
            </TouchableOpacity>
          </View>
          <Button
            labelStyle={styles.buttonTitle}
            style={styles.buttonLogin}
            type={'blue'}
            mode="contained"
            onPress={() => {
              setModalProcess(true);
            }}>
            Lưu
          </Button>
        </Selection>
      </SafeAreaView>
    </Container>
  );
}
