import {DefaultTheme} from 'react-native-paper';
import {StatusBar} from 'react-native';
import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  wrapLogin: {
    marginTop: 16,
    marginBottom: 23,
  },
  wrapFieldInput: {
    paddingTop: 21,
    paddingLeft: 24,
    paddingRight: 25,
    backgroundColor: '#253042',
    borderRadius: 10,
    shadowColor: 'rgba(28, 35, 45, 0.25)',
  },
  inputLogin: {
    height: 44,
    fontSize: 17,
    lineHeight: 22,
    ...SanFrancisco.regular,
    color: '#000000',
  },
  inputeye: {
    position: 'absolute',
    right: 0,
    top: 15,
  },
  buttonTitle: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 28,
    color: '#FFFFFF',
  },
  buttonLogin: {
    borderRadius: 8,
    height: 50,
  },
  textContentLogout: {
    ...SanFrancisco.regular,
    fontSize: 13,
    lineHeight: 16,
    color: Const.BLACK,
    textAlign: 'center',
    marginLeft: 17,
    marginRight: 17,
    marginBottom: 23,
  },
  texthHeaderLogout: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK,
    marginTop: 19,
    textAlign: 'center',
    marginBottom: 2,
  },
  blockModal: {
    backgroundColor: '#ffffff',
    borderRadius: 14,
    width: 270,
  },
  blockBottonOk: {
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: '#004182',
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    paddingTop: 12,
    paddingBottom: 12,
  },
  textButton: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 22,
    color: Const.WHITE,
  },
};
