import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const format = function (val, n = 2, x = 3) {
    if (!val) {
        return 0;
    }
    if (typeof val === 'string') {
        return format(Number(val), n, x);
    }

    if (`${val}`.split('.').length === 1) {
        n = 0;
    }

    const re = `\\d(?=(\\d{${x || 3}})+${n > 0 ? '\\.' : '$'})`;
    return val.toFixed(Math.max(0, n)).replace(new RegExp(re, 'g'), '$&,');
};

function FormatNumber({ value, currency, mark, ...props }) {
    return (
        <Text {...props}>
            {mark}
            {format(value)}
            {currency}
        </Text>
    );
}

FormatNumber.defaultProps = {
    currency: 'đ',
};

FormatNumber.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
};

export default FormatNumber;
