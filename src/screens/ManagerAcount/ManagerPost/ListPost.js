import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import {styles} from '../../../core/Acount/managerpost/styles';
import Header from '../../../components/Header';
import {Container} from 'native-base';
import {TabView} from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import RentalNews from './RentalNews';
import DraftNews from './DraftNews';

export default function ListPost({navigation}) {
  const _handleIndexChange = i => setIndex(i);
  const [index, setIndex] = React.useState(0);
  const renderScene = ({route}) => {
    switch (route.key) {
      case '1':
        return <RentalNews navigation={navigation} />;
      case '2':
        return <DraftNews navigation={navigation} />;
      default:
        return null;
    }
  };
  const [routes] = React.useState([
    {key: '1', title: 'Tin bán/Cho thuê'},
    {key: '2', title: 'Tin nháp'},
  ]);
  const _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View style={[styles.tabBar]}>
        {props.navigationState.routes.map((route, i) => {
          return (
            <TouchableOpacity
              style={[
                styles.tabItem,
                index === i ? {backgroundColor: '#004182'} : {},
              ]}
              onPress={() => setIndex(i)}>
              <Animated.Text
                style={[
                  styles.titleTab,
                  index === i ? {color: '#ffffff'} : {color: '#000000'},
                ]}>
                {route.title}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Quản lý đăng tin" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <TabView
          navigationState={{index, routes}}
          renderScene={renderScene}
          renderTabBar={_renderTabBar}
          onIndexChange={_handleIndexChange}
        />
      </SafeAreaView>
    </Container>
  );
}
