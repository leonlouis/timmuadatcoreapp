import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  //issetInfo
  blockTitle: {
    marginTop: 11,
  },
  infoUser: {
    flexDirection: 'row',
    marginBottom: 14,
  },
  username_info: {
    ...SanFrancisco.semiBold,
    fontSize: 19,
    lineHeight: 23,
    color: Const.BLACK,
  },
  level_info: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 20,
    color: Const.ORANGE,
    paddingTop: 4,
    paddingBottom: 4,
  },
  value_money: {
    ...SanFrancisco.regular,
    fontSize: 16,
    lineHeight: 20,
    color: Const.BLACK,
  },
  vlue: {
    ...SanFrancisco.semiBold,
  },
  info_full: {
    marginLeft: 16,
  },
  blockTitleQLTK: {
    paddingLeft: 16,
    paddingTop: 14,
    paddingBottom: 15,
    backgroundColor: Const.GRAY,
  },
  titleQLTK: {
    ...SanFrancisco.regular,
    fontSize: 13,
    lineHeight: 18,
    color: Const.GRAY_v2,
    textTransform: 'uppercase',
  },
  itemQlTK: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 12.5,
    paddingBottom: 11.5,
  },
  borderItem: {
    borderBottomWidth: 0.5,
    borderBottomColor: Const.GRAY_v2,
  },
  totalNotifi: {
    flexDirection: 'row',
  },
  numberNotifi: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 22,
    color: Const.WHITE,
  },
  blockNumberNotifi: {
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 6,
    paddingRight: 6,
    backgroundColor: Const.RED,
    borderRadius: 12,
    marginRight: 10,
  },
  vecterNext: {
    marginTop: 8,
  },
};
