import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, FlatList} from 'react-native';
import styles from '../../core/Component/listcountry/styles';
import {Users} from '../../models/users';
import Modal from 'react-native-modal';
import SanFrancisco from '../SanFrancisco';
import Const from '../Const';
import Selection from '../Selection';

function ModalProcess({
  visible,
  onChange,
  title,
  content,
  typeButton = 1,
  data,
  ...props
}) {
  const [walletId, setWalletId] = useState(data);
  useEffect(() => {
    if (!visible) {
      return;
    }
  }, [visible]);

  const setWallet = item => {
    onChange(item);
  };

  if (!visible) {
    return <View />;
  }

  return (
    <Modal
      isVisible={visible}
      style={{
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        zIndex: 9999,
      }}>
      <View
        style={{
          backgroundColor: '#ffffff',
          borderRadius: 14,
          width: 270,
        }}>
        <Selection size={16}>
          <Text
            style={{
              ...SanFrancisco.semiBold,
              fontSize: 17,
              lineHeight: 22,
              color: Const.BLACK,
              marginTop: 19,
              textAlign: 'center',
              marginBottom: 2,
            }}>
            {title}
          </Text>
          <Text
            style={{
              ...SanFrancisco.regular,
              fontSize: 13,
              lineHeight: 16,
              color: Const.BLACK,
              textAlign: 'center',
              marginLeft: 17,
              marginRight: 17,
              marginBottom: 23,
            }}>
            {content}
          </Text>
        </Selection>
        {typeButton == '1' ? (
          <TouchableOpacity
            onPress={() => setWallet(1)}
            style={{
              textAlign: 'center',
              alignItems: 'center',
              backgroundColor: '#004182',
              borderBottomLeftRadius: 14,
              borderBottomRightRadius: 14,
              paddingTop: 12,
              paddingBottom: 12,
            }}>
            <Text
              style={{
                ...SanFrancisco.semiBold,
                fontSize: 17,
                lineHeight: 22,
                color: Const.WHITE,
              }}>
              OK
            </Text>
          </TouchableOpacity>
        ) : (
          <View
            style={{
              flexDirection: 'row',
              borderTopWidth: 0.5,
              borderTopColor: '#E6E6E6',
            }}>
            <TouchableOpacity
              onPress={() => setWallet(1)}
              style={{
                paddingTop: 12,
                paddingBottom: 12,
                borderRightWidth: 0.5,
                borderRightColor: '#E6E6E6',
                width: '50%',
              }}>
              <Text
                style={{
                  ...SanFrancisco.regular,
                  fontSize: 17,
                  lineHeight: 22,
                  color: Const.BLUE_v2,
                  textAlign: 'center',
                  alignItems: 'center',
                  alignContent: 'center',
                  alignSelf: 'center',
                }}>
                Xóa
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setWallet(1)}
              style={{
                textAlign: 'center',
                alignItems: 'center',
                paddingTop: 12,
                paddingBottom: 12,
                width: '50%',
              }}>
              <Text
                style={{
                  ...SanFrancisco.semiBold,
                  fontSize: 17,
                  lineHeight: 22,
                  color: Const.BLUE_v2,
                }}>
                Không
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </Modal>
  );
}

export default ModalProcess;
