import {DefaultTheme} from 'react-native-paper';
import {StatusBar} from 'react-native';
import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  blockPost: {
    paddingTop: 16,
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 26,
  },
  buttonTitle: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    // lineHeight: 20,
    color: Const.WHITE,
    textTransform: 'capitalize',
  },
  buttonLogin: {
    borderRadius: 8,
    height: 50,
    borderWidth: 1,
    borderColor: Const.BLUE,
  },
  itemQlTKTP: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#E6E6E6',
    paddingTop: 10,
    marginBottom: 16,
  },
  textLeftInfo: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK_ROOT,
    paddingBottom: 10,
  },
  textLeftInfoNotcontent: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK_ROOT,
  },
  blockAllIcon: {
    flexDirection: 'row',
  },
  vecterNext: {
    marginTop: 18,
  },
  vecterNextNotContent: {
    marginTop: 8,
  },
  titleText: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 20,
    color: Const.GRAY_v5,
  },
  ggMap: {
    width: '100%',
    height: 215,
    marginBottom: 15,
  },
  //step2
  textMtcb: {
    ...SanFrancisco.regular,
    fontSize: 16,
    lineHeight: 20,
    color: Const.BLACK,
    textTransform: 'uppercase',
    marginBottom: 12,
  },
  itemQlTKMXH: {
    flexShrink: 1,
  },
  inputLogin: {
    height: 44,
    fontSize: 17,
    lineHeight: 22,
    ...SanFrancisco.regular,
    color: '#000000',
    textAlign: 'left',
  },
  textTT: {
    ...SanFrancisco.regular,
    fontSize: 14,
    lineHeight: 16,
    color: Const.BLACK,
    textAlign: 'right',
    marginTop: 10,
    marginBottom: 10,
  },
  m2DT: {
    ...SanFrancisco.regular,
    fontSize: 14,
    lineHeight: 16,
    color: Const.BLACK,
    position: 'absolute',
    right: 0,
    top: 22,
  },
  downUp: {
    position: 'absolute',
    right: 0,
    bottom: 20,
  },
  totalPrice: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 20,
    color: Const.BLACK_ROOT,
    marginBottom: 24,
  },
  blockPriceImage: {
    marginTop: 28,
  },
  price: {color: '#429134', marginLeft: 12},
  flex: {
    flexDirection: 'row',
  },
  image: {
    width: 83,
    height: 83,
    borderRadius: 8,
    marginRight: 16,
  },
  blockImage: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 16,
  },
  blockAddImage: {
    backgroundColor: Const.GRAY_v7,
    borderWidth: 1,
    borderColor: Const.GRAY_v6,
    borderRadius: 8,
    textAlign: 'center',
    justifySelf: 'center',
    justifyContent: 'center',
    justifyItems: 'center',
    alignItems: 'center',
    width: 83,
    height: 83,
  },
  imageAdd: {
    ...SanFrancisco.regular,
    fontSize: 13,
    lineHeight: 15,
    color: Const.BLUE,
    marginTop: 8,
  },
  blockNumber: {
    flexDirection: 'row',
  },
  blockInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 12,
  },
  textRightInfo: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK_ROOT,
  },
  numberTr: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK_ROOT,
  },
  blockNumberTr: {
    paddingTop: 5,
    paddingBottom: 4,
    paddingLeft: 12,
    paddingRight: 12,
    backgroundColor: '#7676801F',
    borderRadius: 4,
    width: 42,
    height: 29,
    alignItems: 'center',
  },
};
