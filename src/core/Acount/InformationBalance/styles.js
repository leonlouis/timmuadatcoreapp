import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  buttonTitle: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 22,
    color: Const.WHITE,
  },
  buttonLogin: {
    borderRadius: 8,
    height: 50,
    borderWidth: 1,
    borderColor: Const.BLUE,
    // marginTop: 13,
    marginBottom: 16,
    marginTop: 16,
  },
  //issetInfo
  blockTitleQLTK: {
    paddingTop: 13,
    paddingBottom: 13,
  },
  titleQLTK: {
    ...SanFrancisco.regular,
    fontSize: 15,
    lineHeight: 22,
    color: Const.GREEN,
    textAlign: 'right',
  },

  itemQlTKTPTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#E6E6E6',
    paddingBottom: 12,
    paddingTop: 13,
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: '#E6E6E6',
  },
  itemQlTKTP: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 12,
    paddingTop: 12,
  },
  textLeftInfo: {
    ...SanFrancisco.regular,
    fontSize: 16,
    lineHeight: 20,
    color: Const.GRAY_v5,
  },
  textLeftInfoBottom: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK_ROOT,
  },
  textLeftTotal: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 20,
    color: Const.BLACK_ROOT,
  },
  //
  vecterNext: {
    marginTop: 8,
  },
  itemQlTK: {
    paddingBottom: 10,
    flexShrink: 1,
  },
  textLeftIcon: {
    fontSize: 16,
    lineHeight: 20,
    ...SanFrancisco.regular,
    color: Const.GRAY_v5,
  },
  textLeftIconBottom: {
    fontSize: 17,
    lineHeight: 22,
    ...SanFrancisco.regular,
    color: Const.BLACK_ROOT,
  },
  textLeftIconPriceTotal: {
    fontSize: 17,
    lineHeight: 20,
    ...SanFrancisco.semiBold,
    color: Const.RED,
  },
  blockAllIcon: {
    flexDirection: 'row',
  },
  blockNote: {
    backgroundColor: Const.GRAY_v7,
    padding: 16,
  },
  textNote: {
    fontSize: 14,
    lineHeight: 16,
    ...SanFrancisco.regular,
    color: Const.GRAY_v4,
  },
};
