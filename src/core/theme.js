import {DefaultTheme} from 'react-native-paper';
import {StatusBar} from 'react-native';

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    text: '#000000',
    primary: '#560CCE',
    secondary: '#414757',
    error: '#f13a59',
  },
  container: {
    flex: 1,
    // paddingTop: StatusBar.currentHeight,
    // backgroundColor: '#1C232D',
  },
};
