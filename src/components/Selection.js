import React from 'react';
import {View} from 'react-native';
import PropTypes from 'prop-types';

function Selection({children, style, bgColor, full, size = 20, ...props}) {
  return (
    <View
      style={[
        {
          paddingLeft: full ? 0 : size,
          paddingRight: full ? 0 : size,
          // backgroundColor: bgColor,
        },
        style,
      ]}
      {...props}>
      {children}
    </View>
  );
}

Selection.defaultProps = {
  bgColor: '#fff',
};

Selection.propTypes = {
  children: PropTypes.element.isRequired,
};

export default Selection;
