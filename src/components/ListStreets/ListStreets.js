import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  Dimensions,
} from 'react-native';
import SanFrancisco from '../SanFrancisco';
import Const from '../Const';
import Selection from '../Selection';
import {DashboardModel} from '../../models/dashboard';
import {styles} from '../../core/dashboard/styles';
import TextInput from '../TextInput';
import FormatNumber from '../FormatNumber';
import Slug from '../Slug';
import {LocationModel} from '../../models/location';

const {width, height} = Dimensions.get('screen');

function ListStreets({
  visible,
  onChange,
  data,
  keyFilterCity,
  keyFilterDisctrict,
  header,
  ...props
}) {
  const [filterObject, setFilterObject] = useState('');
  const [object, setObject] = useState(data || '');
  const [listPlaceData, setListPlaceData] = useState([]);
  const [listPlaceDataRoot, setListPlaceDataRoot] = useState([]);
  const renderItem = ({item}) => (
    <TouchableOpacity
      style={{
        borderBottomWidth: 1,
        borderBottomColor: '#E6E6E6',
        paddingTop: 12,
        paddingBottom: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}
      onPress={() => setObject(item)}>
      <Text
        style={[
          {
            ...SanFrancisco.regular,
            fontSize: 17,
            lineHeight: 22,
            color: Const.BLACK_ROOT,
          },
          item.id === object.id
            ? {color: Const.BLUE}
            : {
                color: Const.BLACK_ROOT,
              },
        ]}>
        {item.name}
      </Text>
      {item.id === object.id ? (
        <Image source={require('../../assets/post/Symbol.png')} />
      ) : null}
    </TouchableOpacity>
  );
  useEffect(() => {
    if (!visible) {
      return;
    }
    setObject(data);
    setListPlaceData([]);
    setListPlaceDataRoot([]);
    LocationModel.getListStreetsApi(keyFilterCity, keyFilterDisctrict)
      .then(data => {
        if (data.status == 1) {
          setListPlaceData([
            {
              name: 'Tất cả',
              id: '',
            },
            ...data.data,
          ]);
          setListPlaceDataRoot([
            {
              name: 'Tất cả',
              id: '',
            },
            ...data.data,
          ]);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }, [visible]);

  const setWallet = () => {
    onChange(object);
  };

  if (!visible) {
    return <View />;
  }
  const onChangeHandler = () => {
    let Search = filterObject;
    if (Search !== '') {
      //const b = listPlaceData.filter(item => Slug(item.name) === Slug(Search));
      const b = listPlaceData.filter(
        item =>
          item &&
          item.name &&
          item.name.toLowerCase().includes(Search.toLowerCase()),
      );
      setListPlaceData(b);
    } else {
      setListPlaceData(listPlaceDataRoot);
    }
  };

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        zIndex: 9999999,
        position: 'absolute',
      }}>
      <View
        style={{
          backgroundColor: '#ffffff',
          height: '100%',
          width: '100%',
        }}>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            marginBottom: 16,
            flexDirection: 'row',
            justifyContent: 'space-between',
            height: 44,
            backgroundColor: '#F9F9F9F0',
            paddingTop: 12,
            paddingBottom: 12,
            paddingLeft: 16,
            paddingRight: 16,
          }}>
          <TouchableOpacity onPress={() => setWallet()}>
            <Image source={require('../../assets/back.png')} />
          </TouchableOpacity>
          <Text
            style={{
              ...SanFrancisco.semiBold,
              fontSize: 17,
              lineHeight: 20,
              color: Const.BLACK_ROOT,
            }}>
            {header}
          </Text>
          <TouchableOpacity onPress={() => setWallet()}>
            <Text
              style={{
                ...SanFrancisco.regular,
                fontSize: 17,
                lineHeight: 20,
                color: Const.BLUE,
              }}>
              Xong
            </Text>
          </TouchableOpacity>
        </View>
        <Selection size={16} style={{marginBottom: 120}}>
          <View style={{paddingBottom: 8}}>
            <TouchableOpacity
              onPress={() => {
                onChangeHandler();
              }}
              style={{
                position: 'absolute',
                padding: 10,
                paddingRight: 4,
                zIndex: 9999,
              }}>
              <Image
                source={require('../../assets/dashboard/SearchGlyphLight.png')}
              />
            </TouchableOpacity>
            <TextInput
              style={[
                {
                  backgroundColor: '#7676801F',
                  height: 44,
                  paddingLeft: 30,
                  fontSize: 17,
                  letterSpacing: -0.3,
                  ...SanFrancisco.regular,
                  color: '#3C3C4399',
                  borderTopRightRadius: 10,
                  borderTopLeftRadius: 10,
                  borderBottomRightRadius: 10,
                  borderBottomLeftRadius: 10,
                  textAlign: 'left',
                },
              ]}
              underlineColor="transparent"
              placeholder="Tìm kiếm..."
              placeholderTextColor="#999999"
              returnKeyType="next"
              // value={filterObject.value}
              onChangeText={text => setFilterObject(text)}
              // error={!!filterObject.error}
              // errorText={filterObject.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              autoCompleteType="email"
              textContentType="emailAddress"
              keyboardType="email-address"
              marginVerNone={0}
            />
          </View>
          <FlatList data={listPlaceData} renderItem={renderItem} />
        </Selection>
      </View>
    </View>
  );
}

export default ListStreets;
