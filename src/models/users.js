import {config} from '../config/config';

export const Users = {
  getUserInfoByToken: token => {
    return fetch(config.endpointUrl + 'api/user/get_info', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    }).then(data => data.json());
  },
  login: (username, password) => {
    return fetch(config.endpointUrl + 'api/authorize/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': token,
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    }).then(data => data.json());
  },
  register: (
    account_type,
    username,
    email,
    password,
    re_password,
    full_name,
    phone,
    dob,
    gender,
    province_id,
    district_id,
    ward_id,
  ) => {
    return fetch(config.endpointUrl + 'api/authorize/register', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //'Authorization': token,
      },
      body: JSON.stringify({
        account_type: account_type,
        username: username,
        email: email,
        password: password,
        re_password: re_password,
        full_name: full_name,
        phone: phone,
        dob: dob,
        gender: gender,
        province_id: province_id,
        district_id: district_id,
        ward_id: ward_id,
      }),
    }).then(data => data.json());
  },
  change_password: (old_password, password, re_password) => {
    return fetch(config.endpointUrl + 'api/user/change_password', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        old_password: old_password,
        password: password,
        re_password: re_password,
      }),
    }).then(data => data.json());
  },
  uploadFile: responseImg => {
    const formData = new FormData();
    formData.append('avatar', {
      uri: responseImg.uri.replace('file://', ''),
      name: responseImg.fileName || responseImg.uri.split('/').pop(),
      type: responseImg.type,
    });
    return fetch(config.endpointUrl + 'api/user/update_avatar', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        Authorization: global.token,
      },
      body: formData,
    }).then(data => data.json());
  },
  list_countryAction: () => {
    return fetch(config.endpointUrl + 'api/user/list_country', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
};
