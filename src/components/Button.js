import React from 'react';
import {StyleSheet} from 'react-native';
import {Button as PaperButton} from 'react-native-paper';
const renderColor = type => {
  if (type === 'danger') {
    return '#ED1B24';
  }
  if (type === 'default') {
    return '#C4C4C4';
  }
  if (type === 'primary') {
    return '#19769F';
  }
  if (type === 'yellow') {
    return '#FF9933';
  }
  if (type === 'green') {
    return '#04D08C';
  }
  if (type === 'blue') {
    return '#004182';
  }
  return '#fff';
};

export default function Button({mode, style, type, ...props}) {
  return (
    <PaperButton
      style={[styles.button, {backgroundColor: renderColor(type)}, style]}
      mode={mode}
      {...props}
    />
  );
}

const styles = StyleSheet.create({
  button: {
    width: '100%',
    marginVertical: 10,
    paddingVertical: 2,
  },
  text: {
    fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 26,
  },
});
