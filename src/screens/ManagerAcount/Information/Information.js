import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
  Alert,
  AsyncStorage,
} from 'react-native';
import {Text} from 'react-native-paper';
import Button from '../../../components/Button';
import {styles} from '../../../core/Acount/information/styles';
import {passwordValidator} from '../../../helpers/passwordValidator';
import {Users} from '../../../models/users';
import Header from '../../../components/Header';
import Selection from '../../../components/Selection';
import {Container} from 'native-base';
import {nameValidator} from '../../../helpers/nameValidator';

export default function Information({navigation}) {
  const [email, setEmail] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});
  const [issetInfo, setIssetInfo] = useState(false);

  function logOut() {
    AsyncStorage.removeItem('user_token')
      .then(() => {
        global.token = null;
        global.userinfo = {};
        navigation.reset({
          index: 0,
          routes: [{name: 'Login'}],
        });
      })
      .done();
  }

  const onLoginPressed = () => {
    const emailError = nameValidator(email.value);
    const passwordError = passwordValidator(password.value);
    setEmail({...email, error: emailError});
    setPassword({...password, error: passwordError});
    if (!emailError && !passwordError) {
      Users.login(email.value, password.value)
        .then(data => {
          if (data.status == 1) {
            AsyncStorage.setItem('user_token', data.optional.token);
            global.token = data.optional.token;
            global.userinfo = data.data;
            navigation.navigate('Dashboard', {object: ''});
          } else {
            Alert.alert('Notify', data.message);
          }
        })
        .catch(e => {
          navigation.navigate('Dashboard', {object: {}});
        });
    }
  };
  const fillGender = item => {
    if (item == 1) {
      return 'Nam';
    } else if (item == 2) {
      return 'Nữ';
    } else {
      return 'Chưa nhập';
    }
  };
  const fillType = item => {
    if (item == 1) {
      return 'Cá nhân';
    } else if (item == 2) {
      return 'Doanh nghiệp';
    } else {
      return 'Chưa nhập';
    }
  };
  return (
    <Container style={{flex: 1}}>
      <Header
        keyHeader="Thông tin tài khoản"
        navigation={navigation}
        iconRight={'text'}
        nameNavigation={'UpdateInformation'}
      />
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <Selection size={16} style={styles.blockTitle}>
            <View style={styles.infoUser}>
              {global?.userinfo?.avatar ? (
                <Image
                  source={{
                    uri: global?.userinfo?.avatar,
                  }}
                  style={{
                    width: 74,
                    height: 74,
                    borderRadius: 50,
                  }}
                />
              ) : (
                <Image
                  source={require('../../../assets/ic_authorize/icon_login/MaskGroup309.png')}
                />
              )}
              <View style={styles.info_full}>
                <Text style={styles.username_info}>
                  {global?.userinfo?.username}
                </Text>
                <Text style={styles.value_money}>
                  {fillType(global?.userinfo?.account_type)}
                </Text>
                <Text style={styles.level_info}>Tài khoản VIP</Text>
              </View>
            </View>
          </Selection>
          <View>
            <View style={styles.blockTitleQLTK}>
              <Text style={styles.titleQLTK}>Thông tin cá nhân</Text>
            </View>
            <Selection size={16} style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Họ và tên</Text>
              <Text style={styles.textRightInfo}>
                {global?.userinfo?.username}
              </Text>
            </Selection>
            <Selection size={16} style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Ngày sinh</Text>
              <Text style={styles.textRightInfo}>
                {global?.userinfo?.birthday}
              </Text>
            </Selection>
            <Selection size={16} style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Giới tính</Text>
              <Text style={styles.textRightInfoNodata}>
                {fillGender(global?.userinfo?.gender)}
              </Text>
            </Selection>
          </View>
          <View>
            <View style={styles.blockTitleQLTK}>
              <Text style={styles.titleQLTK}>Thông tin liên hệ</Text>
            </View>
            <Selection size={16} style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Số điện thoại chính</Text>
              <Text style={styles.textRightInfo}>
                {global?.userinfo?.phone}
              </Text>
            </Selection>
            {/*<Selection size={16} style={styles.itemQlTK}>*/}
            {/*  <Text style={styles.textLeftInfo}>Số điện thoại 2</Text>*/}
            {/*  <Text style={styles.textRightInfo}>09779634163</Text>*/}
            {/*</Selection>*/}
            <Selection size={16} style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Email</Text>
              <Text style={styles.textRightInfoNodata}>
                {global?.userinfo?.email}
              </Text>
            </Selection>
            <Selection size={16} style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Mã số thuế/CMND</Text>
              <Text style={styles.textRightInfoNodata}>
                {global?.userinfo?.cmnd ? global?.userinfo?.cmnd : 'Chưa nhập'}
              </Text>
            </Selection>
            <Selection size={16} style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Facebook</Text>
              <Text style={styles.textRightInfoNodata}>
                {global?.userinfo?.facebook
                  ? global?.userinfo?.facebook
                  : 'Chưa kết nối'}
              </Text>
            </Selection>
            <Selection size={16} style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Twitter</Text>
              <Text style={styles.textRightInfoNodata}>
                {global?.userinfo?.twitter
                  ? global?.userinfo?.twitter
                  : 'Chưa kết nối'}
              </Text>
            </Selection>
            <Selection size={16} style={styles.itemQlTK}>
              <Text style={styles.textLeftInfo}>Zalo</Text>
              <Text style={styles.textRightInfoNodata}>
                {global?.userinfo?.zalo
                  ? global?.userinfo?.zalo
                  : 'Chưa kết nối'}
              </Text>
            </Selection>
          </View>
        </ScrollView>
      </SafeAreaView>
      <Selection size={16}>
        <Button
          labelStyle={styles.buttonTitle}
          style={styles.buttonLogin}
          mode="contained"
          onPress={() => logOut()}>
          Đăng xuất
        </Button>
      </Selection>
    </Container>
  );
}
