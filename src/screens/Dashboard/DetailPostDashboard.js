import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
} from 'react-native';
import {styles} from '../../core/dashboard/Detailpost/styles';
import {Container} from 'native-base';
import Selection from '../../components/Selection';
import {SliderBox} from 'react-native-image-slider-box';
import Header from '../../components/Header';
const {width, height} = Dimensions.get('screen');
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
export default function DetailPostDashboard({navigation}) {
  const [indexTotal, setIndexTotal] = useState(1);
  const [slider, setSlider] = useState([
    require('../../assets/dashboard/Group2391.png'),
    require('../../assets/dashboard/Group2391.png'),
    require('../../assets/dashboard/Group2391.png'),
    require('../../assets/dashboard/Group2391.png'),
    require('../../assets/dashboard/Group2391.png'),
    require('../../assets/dashboard/Group2391.png'),
    require('../../assets/dashboard/Group2391.png'),
    require('../../assets/dashboard/Group2391.png'),
    require('../../assets/dashboard/Group2391.png'),
    require('../../assets/dashboard/Group2391.png'),
  ]);
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Chi tiết tin đăng" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <ScrollView>
          {/*<View style={styles.BlockIconBackElip}>*/}
          {/*  <TouchableOpacity*/}
          {/*    style={styles.iconLeft}*/}
          {/*    onPress={() => {*/}
          {/*      navigation.goBack();*/}
          {/*    }}>*/}
          {/*    <Image*/}
          {/*      source={require('../../assets/dashboard/Post/Chevron.png')}*/}
          {/*    />*/}
          {/*  </TouchableOpacity>*/}
          {/*  <Image*/}
          {/*    source={require('../../assets/dashboard/Post/more.png')}*/}
          {/*    style={styles.iconRight}*/}
          {/*  />*/}
          {/*</View>*/}
          <View style={styles.blockCount}>
            <Text style={styles.totalCount}>
              {indexTotal}/{slider.length}
            </Text>
          </View>
          <SliderBox
            images={slider}
            resizeMethod={'resize'}
            resizeMode={'cover'}
            paginationBoxVerticalPadding={5}
            currentImageEmitter={index => {
              setTimeout(() => {
                setIndexTotal(index + 1);
              }, 400);
            }}
            sliderBoxHeight={210}
          />
          <Selection size={16}>
            <Text style={styles.time}>10 phút trước</Text>
            <Text style={styles.title}>
              Chính chủ bán nhanh căn biệt thự song lập Ngọc Trai 151.3m2,
              Vinhomes Ocean Park. LH 0915972886
            </Text>
            <Text style={styles.price}>1.65 tỷ</Text>
            <Text style={styles.price_dt}>
              Đường Ngọc Trai, Xã Dương Xá, Gia Lâm, Hà Nội
            </Text>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Maps');
              }}
              style={styles.blockMap}>
              <Image
                source={require('../../assets/dashboard/Post/Path1270.png')}
              />
              <Text style={styles.textMap}>Xem bản đồ</Text>
            </TouchableOpacity>
            <View>
              <Text style={styles.textMT}>Mô tả</Text>
              <Text style={styles.contentMT}>
                Chính chủ cần bán nhanh biệt thự song lập khu Ngọc Trai dự án
                Vinhomes Ocean Park, Gia Lâm, Hà Nội. Diện tích đất: 151.3m2.
                Diện tích nhà xây: 77m2/sàn. Xây dựng 3 tầng + 01 tum. Nhà gần
                hồ lớn 24,5ha, vườn hoa, bể bơi nội khu, khu phức hợp thể thao.
                Là khu khép kín duy nhất của dự án, an ninh tuyệt đối an toàn.
                Giá bán: Liên hệ trực tiếp.
              </Text>
              <Text style={styles.ShowHidden}>Hiển thị thêm</Text>
            </View>
            <Text style={styles.titleInfo}>Thông tin chi tiết</Text>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Loại tin rao</Text>
              <Text style={styles.newRight}>Nhà đất bán</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Loại bất động sản</Text>
              <Text style={styles.newRight}>Bán căn hộ chung cư</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Diện tích</Text>
              <Text style={styles.newRight}>82.4 m2</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Địa chỉ</Text>
              <Text style={styles.newRight}>
                S1-06 Vinhomes Ocean Park, Đa Tốn, Gia lâm, Hà Nội
              </Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Mức giá</Text>
              <Text style={styles.newRight}>~32.2 triệu/m2</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Phòng ngủ</Text>
              <Text style={styles.newRight}>3 PN</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Hướng nhà</Text>
              <Text style={styles.newRight}>Tây - Nam</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Mã tin đăng</Text>
              <Text style={styles.newRight}>330561</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Loại tin đăng</Text>
              <Text style={styles.newRight}>Tin Vip 3</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Ngày đăng tin</Text>
              <Text style={styles.newRight}>06/10/2021</Text>
            </View>
            <View style={styles.blockNews}>
              <Text style={styles.newLeft}>Ngày hết hạn</Text>
              <Text style={styles.newRight}>13/10/2021</Text>
            </View>
          </Selection>
          <MapView
            style={styles.ggMap}
            initialRegion={{
              latitude: 21.0277644,
              longitude: 105.8341598,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            showsUserLocation
            minZoomLevel={11}
          />
          <Selection size={16}>
            <View style={styles.blockLH}>
              <Text style={styles.titleLh}>Liên hệ người đăng tin</Text>
              <View style={styles.blockUserLH}>
                <Image
                  source={require('../../assets/dashboard/Post/MaskGroup295.png')}
                />
                <View style={styles.NamePhone}>
                  <Text style={styles.NameLh}>Mr Nam</Text>
                  <Text style={styles.PhoneLh}>0915972886</Text>
                </View>
              </View>
            </View>
            <View>
              <Text style={styles.titleKV}>Tin đăng cùng khu vực</Text>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('DetailPostDashboard');
                }}
                style={styles.blockImage_header}>
                <View>
                  <Image
                    source={require('../../assets/dashboard/Group2391.png')}
                    style={styles.w100}
                  />
                  <View style={styles.hotnew}>
                    <Text style={styles.vluenew}>Nổi bật</Text>
                  </View>
                  <View style={styles.totalImage}>
                    <Text style={styles.totalIm}>16</Text>
                    <Image
                      source={require('../../assets/dashboard/bx-image.png')}
                    />
                  </View>
                  <View style={styles.totalImage_phone}>
                    <Image
                      source={require('../../assets/dashboard/bx-phone-call.png')}
                    />
                    <Text style={styles.totalIm_phone}>0912 310 691</Text>
                  </View>
                </View>
                <View style={styles.blockItemNew}>
                  <Image
                    source={require('../../assets/dashboard/Path616.png')}
                    style={styles.star5}
                  />
                  <Text style={styles.titleNew}>
                    {'      '}
                    Tổng hợp các căn hộ cần chuyển nhượng giá gốc Vinhomes Ocean
                    Park, lien hệ số 0912310691, hỗ trợ 24/7
                  </Text>
                  <View style={styles.price_time}>
                    <Text style={styles.price_m}>2.65 tỷ · 82.4 m2</Text>
                    <Text style={styles.time_m}>10 phút trước</Text>
                  </View>
                  <Text style={styles.price_m}>Gia Lâm, Hà Nội</Text>
                </View>
              </TouchableOpacity>
              <View style={styles.blockImage_header}>
                <View style={styles.blockItemNew}>
                  <Image
                    source={require('../../assets/dashboard/Path616.png')}
                    style={styles.star5}
                  />
                  <Text style={styles.titleNew}>
                    {'      '}
                    Cần bán hoặc cho thuê căn hộ 2PN giá tốt tòa nhà D'Qua Nha
                    Trang. LH: 090390302
                  </Text>
                  <View style={styles.blockItemW}>
                    <View>
                      <Image
                        source={require('../../assets/dashboard/Group2391.png')}
                        style={styles.wblock}
                      />
                      <View style={styles.totalImage}>
                        <Text style={styles.totalIm}>8</Text>
                        <Image
                          source={require('../../assets/dashboard/bx-image.png')}
                        />
                      </View>
                    </View>
                    <View>
                      <Text style={styles.price_m_w}>
                        1.65 tỷ · 75 m² · 2 PN · 2 WC
                      </Text>
                      <Text style={[styles.price_m, {marginBottom: 12}]}>
                        Gia Lâm, Hà Nội
                      </Text>
                      <Text style={styles.time_m}>Hôm nay</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.blockImage_header}>
                <View style={styles.blockItemNew}>
                  <Image
                    source={require('../../assets/dashboard/Path616.png')}
                    style={styles.star5}
                  />
                  <Text style={styles.titleNew}>
                    {'      '}
                    Cần bán hoặc cho thuê căn hộ 2PN giá tốt tòa nhà D'Qua Nha
                    Trang. LH: 090390302
                  </Text>
                  <View style={styles.blockItemW}>
                    <View>
                      <Image
                        source={require('../../assets/dashboard/Group2391.png')}
                        style={styles.wblock}
                      />
                      <View style={styles.totalImage}>
                        <Text style={styles.totalIm}>8</Text>
                        <Image
                          source={require('../../assets/dashboard/bx-image.png')}
                        />
                      </View>
                    </View>
                    <View>
                      <Text style={styles.price_m_w}>
                        1.65 tỷ · 75 m² · 2 PN · 2 WC
                      </Text>
                      <Text style={[styles.price_m, {marginBottom: 12}]}>
                        Gia Lâm, Hà Nội
                      </Text>
                      <Text style={styles.time_m}>Hôm nay</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </Selection>
        </ScrollView>
      </SafeAreaView>
      <TouchableOpacity style={styles.callIcon}>
        <Image source={require('../../assets/dashboard/Post/Path1272.png')} />
      </TouchableOpacity>
    </Container>
  );
}
