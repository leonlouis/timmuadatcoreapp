import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
  Alert,
  AsyncStorage,
  FlatList,
} from 'react-native';
import {Text} from 'react-native-paper';
import Button from '../../../components/Button';
import {styles} from '../../../core/Acount/FilterPost/styles';
import Header from '../../../components/Header';
import Selection from '../../../components/Selection';
import {Container} from 'native-base';
import TextInput from '../../../components/TextInput';
import Option from '../../../components/Option';
import PopupListCountry from '../../../components/ListPopup/ListCountry';
import ModalProcess from '../../../components/ListModal/ModalProcess';
import Const from '../../../components/Const';

export default function FilterPost({navigation}) {
  const [phone, setPhone] = useState({value: '', error: ''});
  const [listPhone, setListPhone] = useState([{}]);
  const [type, setType] = useState(2);

  const [popupListCountry, setPopupListCountry] = useState(false);
  const [modalProcess, setModalProcess] = useState(false);
  const [from_country, setFrom_country] = useState('');
  const [from_country_name, setFrom_country_name] = useState('');
  const setCloseCountry = item => {
    setFrom_country(item.value);
    setFrom_country_name(item.name);
    setPopupListCountry(false);
  };
  const setCloseModal = item => {
    setModalProcess(false);
  };

  return (
    <Container style={{flex: 1}}>
      <Header
        keyHeader="Lọc đăng tin"
        navigation={navigation}
        type={'Noback'}
        iconRight={'icon'}
      />
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <Selection size={16}>
            <TouchableOpacity style={styles.blockTitleQLTK}>
              <Text style={styles.titleQLTK}>Xóa bộ lọc</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setPopupListCountry(true)}
              style={[styles.itemQlTKTP, {marginTop: 0}]}>
              <View>
                <Text style={styles.textLeftInfo}>Từ ngày</Text>
              </View>
              <View style={styles.blockAllIcon}>
                <Text style={[styles.textLeftIcon, {color: Const.GRAY_v5}]}>
                  01/06/2021
                </Text>
                <Image
                  source={require('../../../assets/Acount/filterpost/bx-calendar.png')}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setPopupListCountry(true)}
              style={[styles.itemQlTKTP]}>
              <View>
                <Text style={styles.textLeftInfo}>Đến ngày</Text>
              </View>
              <View style={styles.blockAllIcon}>
                <Text style={[styles.textLeftIcon, {color: Const.GRAY_v5}]}>
                  01/06/2021
                </Text>
                <Image
                  source={require('../../../assets/Acount/filterpost/bx-calendar.png')}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setPopupListCountry(true)}
              style={[styles.itemQlTKTP]}>
              <View>
                <Text style={styles.textLeftInfo}>Loại tin rao</Text>
              </View>
              <View style={styles.blockAllIcon}>
                <Text style={styles.textLeftIcon}>Tất cả</Text>
                <Image
                  source={require('../../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setPopupListCountry(true)}
              style={[styles.itemQlTKTP]}>
              <View>
                <Text style={styles.textLeftInfo}>Loại bất động sản</Text>
              </View>
              <View style={styles.blockAllIcon}>
                <Text style={styles.textLeftIcon}>Tất cả</Text>
                <Image
                  source={require('../../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setPopupListCountry(true)}
              style={[styles.itemQlTKTP]}>
              <View>
                <Text style={styles.textLeftInfo}>Tỉnh/Thành phố</Text>
              </View>
              <View style={styles.blockAllIcon}>
                <Text style={styles.textLeftIcon}>Trên toàn quốc</Text>
                <Image
                  source={require('../../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setPopupListCountry(true)}
              style={[styles.itemQlTKTP]}>
              <View>
                <Text style={styles.textLeftInfo}>Loại tin đăng</Text>
              </View>
              <View style={styles.blockAllIcon}>
                <Text style={styles.textLeftIcon}>Tin thường</Text>
                <Image
                  source={require('../../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setPopupListCountry(true)}
              style={[styles.itemQlTKTP]}>
              <View>
                <Text style={styles.textLeftInfo}>Trạng thái</Text>
              </View>
              <View style={styles.blockAllIcon}>
                <Text style={styles.textLeftIcon}>Tất cả</Text>
                <Image
                  source={require('../../../assets/Vector.png')}
                  style={styles.vecterNext}
                />
              </View>
            </TouchableOpacity>
          </Selection>
        </ScrollView>
      </SafeAreaView>
      <Selection size={16}>
        <Button
          labelStyle={styles.buttonTitle}
          style={styles.buttonLogin}
          type={'blue'}
          mode="contained">
          Áp dụng
        </Button>
      </Selection>
      <PopupListCountry
        visible={popupListCountry}
        data={from_country}
        onChange={item => setCloseCountry(item)}
      />
    </Container>
  );
}
