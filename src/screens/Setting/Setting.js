import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  FlatList,
  AsyncStorage,
} from 'react-native';
import {styles} from '../../core/setting/styles';
import {MainBottomTabComponent} from '../../components/BottomTab';
import {Body, Container, Text} from 'native-base';

import Button from '../../components/Button';

export default function Setting({navigation}) {
  function logOut() {
    AsyncStorage.removeItem('user_token')
      .then(() => {
        global.token = null;
        global.userinfo = {};
        navigation.reset({
          index: 0,
          routes: [{name: 'Login'}],
        });
      })
      .done();
  }

  const [isEnabled, setIsEnabled] = useState(false);
  const handleUnBlock = item => {
    setIsEnabled(!item);
  };
  return (
    <Container style={{flex: 1}}>
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <Text>Setting</Text>
          <Button
            onPress={() => logOut()}
            labelStyle={styles.buttonTitle}
            type="yellow"
            style={styles.buttonBTN}
            mode="contained">
            SIGN OUT
          </Button>
        </SafeAreaView>
      </ScrollView>
      <MainBottomTabComponent navigation={navigation} activeTab="setting" />
    </Container>
  );
}
