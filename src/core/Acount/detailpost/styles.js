import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  imageItem: {
    width: '100%',
    height: 210,
  },
  title: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 24,
    color: Const.BLACK,
    marginTop: 16,
  },
  price_dt: {
    ...SanFrancisco.regular,
    fontSize: 16,
    lineHeight: 21,
    color: Const.BLACK,
    marginTop: 16,
    marginBottom: 16,
  },
  blockNews: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 12,
    flexShrink: 1,
  },
  newLeft: {
    ...SanFrancisco.regular,
    fontSize: 16,
    lineHeight: 24,
    color: Const.GRAY_v4,
    flexShrink: 1,
  },
  newRight: {
    ...SanFrancisco.regular,
    fontSize: 16,
    lineHeight: 24,
    color: Const.BLACK,
    flexShrink: 1,
    width: '50%',
  },
  blockItem: {
    flexDirection: 'row',
    width: '50%',
    textAlign: 'center',
    justifySelf: 'center',
    justifyContent: 'center',
  },
  blockEditTrash: {
    flexDirection: 'row',
    backgroundColor: '#F9F9F9F0',
    paddingTop: 16,
    paddingBottom: 13,
  },
  textItem: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 20,
    color: Const.BLACK,
    marginLeft: 12,
  },
};
