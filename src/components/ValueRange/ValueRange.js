import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, FlatList, Image} from 'react-native';
import SanFrancisco from '../SanFrancisco';
import Const from '../Const';
import Selection from '../Selection';
import {DashboardModel} from '../../models/dashboard';

function ValueRange({visible, onChange, data, keyFilter, header, ...props}) {
  const [object, setObject] = useState(data || '');
  const [listPlaceData, setListPlaceData] = useState([]);
  const renderItem = ({item}) => (
    <TouchableOpacity
      style={{
        borderBottomWidth: 1,
        borderBottomColor: '#E6E6E6',
        paddingTop: 12,
        paddingBottom: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}
      onPress={() => setObject(item)}>
      <Text
        style={[
          {
            ...SanFrancisco.regular,
            fontSize: 17,
            lineHeight: 22,
            color: Const.BLACK_ROOT,
          },
          item.id === object.id
            ? {color: Const.BLUE}
            : {
                color: Const.BLACK_ROOT,
              },
        ]}>
        {item.name}
      </Text>
      {item.id === object.id ? (
        <Image source={require('../../assets/post/Symbol.png')} />
      ) : null}
    </TouchableOpacity>
  );
  useEffect(() => {
    if (!visible) {
      return;
    }
    setObject(data);
    setListPlaceData([]);
    DashboardModel.getPriceConditions(keyFilter)
      .then(data => {
        if (data.status == 1) {
          setListPlaceData([
            {
              name: 'Tất cả',
              id: '',
            },
            ...data.data,
          ]);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }, [visible]);

  const setWallet = () => {
    onChange(object);
  };

  if (!visible) {
    return <View />;
  }

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        zIndex: 9999999,
        position: 'absolute',
      }}>
      <View
        style={{
          backgroundColor: '#ffffff',
          height: '100%',
          width: '100%',
        }}>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            marginBottom: 16,
            flexDirection: 'row',
            justifyContent: 'space-between',
            height: 44,
            backgroundColor: '#F9F9F9F0',
            paddingTop: 12,
            paddingBottom: 12,
            paddingLeft: 16,
            paddingRight: 16,
          }}>
          <TouchableOpacity onPress={() => setWallet()}>
            <Image source={require('../../assets/back.png')} />
          </TouchableOpacity>
          <Text
            style={{
              ...SanFrancisco.semiBold,
              fontSize: 17,
              lineHeight: 20,
              color: Const.BLACK_ROOT,
            }}>
            {header}
          </Text>
          <TouchableOpacity onPress={() => setWallet()}>
            <Text
              style={{
                ...SanFrancisco.regular,
                fontSize: 17,
                lineHeight: 20,
                color: Const.BLUE,
              }}>
              Xong
            </Text>
          </TouchableOpacity>
        </View>
        <Selection size={16} style={{marginBottom: 120}}>
          <FlatList data={listPlaceData} renderItem={renderItem} />
        </Selection>
      </View>
    </View>
  );
}

export default ValueRange;
