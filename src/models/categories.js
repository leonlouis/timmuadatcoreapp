import {config} from '../config/config';

export const CategoriesModel = {
  getlistCategoriesAction: filter => {
    return fetch(config.endpointUrl + 'api/categories?type=' + filter, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    }).then(data => data.json());
  },
};
