import React from 'react';
import {theme} from './src/core/theme';
import {Provider} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from './src/screens/SplashScreen';
import Login from './src/screens/Authorize/Login';
import Dashboard from './src/screens/Dashboard/Dashboard';
import Register from './src/screens/Authorize/Register';
import Setting from './src/screens/Setting/Setting';
import UpdateInformation from './src/screens/ManagerAcount/Information/UpdateInformation';
import AddPhoneNumber from './src/screens/ManagerAcount/Information/AddPhoneNumber';
import ChangePassword from './src/screens/Authorize/ChangePassword';
import ManagerInformation from './src/screens/ManagerAcount/Information/ManagerInformation';
import ListPost from './src/screens/ManagerAcount/ManagerPost/ListPost';
import DetailPost from './src/screens/ManagerAcount/ManagerPost/DetailPost';
import FilterPost from './src/screens/ManagerAcount/ManagerPost/FilterPost';
import InformationBalance from './src/screens/ManagerAcount/ManagerTransaction/InformationBalance';
import Information from './src/screens/ManagerAcount/Information/Information';
import DetailPostDashboard from './src/screens/Dashboard/DetailPostDashboard';
import Maps from './src/screens/Maps/Maps';
import Post from './src/screens/Post/Post';
import PostStep_1 from './src/screens/Post/PostStep_1';
import FilterPostDashboard from './src/screens/Dashboard/FilterPostDashboard';
import Search from './src/screens/Dashboard/Search';

const Stack = createStackNavigator();
export default function App() {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="SplashScreen"
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Dashboard" component={Dashboard} />
          <Stack.Screen name="Setting" component={Setting} />
          <Stack.Screen name="Information" component={Information} />
          <Stack.Screen
            name="UpdateInformation"
            component={UpdateInformation}
          />
          <Stack.Screen name="AddPhoneNumber" component={AddPhoneNumber} />
          <Stack.Screen name="ChangePassword" component={ChangePassword} />
          <Stack.Screen
            name="ManagerInformation"
            component={ManagerInformation}
          />
          <Stack.Screen name="ListPost" component={ListPost} />
          <Stack.Screen name="DetailPost" component={DetailPost} />
          <Stack.Screen name="FilterPost" component={FilterPost} />
          <Stack.Screen
            name="FilterPostDashboard"
            component={FilterPostDashboard}
          />
          <Stack.Screen
            name="InformationBalance"
            component={InformationBalance}
          />
          <Stack.Screen
            name="DetailPostDashboard"
            component={DetailPostDashboard}
          />
          <Stack.Screen name="Maps" component={Maps} />
          <Stack.Screen name="Post" component={Post} />
          <Stack.Screen name="PostStep_1" component={PostStep_1} />
          <Stack.Screen name="Search" component={Search} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
