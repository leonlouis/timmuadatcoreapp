import {DefaultTheme} from 'react-native-paper';
import {StatusBar} from 'react-native';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#1C232D',
  },
  wrapHeader: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#253042',
    height: 42,
    marginBottom: 18,
  },
  titleHeader: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 19,
    color: '#FFFFFF',
  },
  wrapListSetting: {
    backgroundColor: '#253042',
    marginBottom: 12,
  },
  itemTitleItem: {
    flexDirection: 'row',
    paddingTop: 12,
  },
  textItem: {
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 17,
    letterSpacing: -0.3,
    color: '#FFFFFF',
    marginLeft: 14,
    marginTop: 3,
  },
  blockItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 11,
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(255, 255, 255, 0.3)',
  },
  iconNext: {
    marginTop: 15,
    marginRight: 4,
  },
  switchOnOff: {
    marginTop: 13,
    marginRight: 4,
  },
  textSecurity: {
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 17,
    letterSpacing: -0.3,
    color: '#FFFFFF',
    marginBottom: 9,
  },
  buttonTitle: {
    // ...SanFrancisco.medium,
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 17,
    color: '#FFFFFF',
    letterSpacing: -0.3,
  },
  buttonBTN: {
    borderRadius: 10,
    height: 40,
  },
};
