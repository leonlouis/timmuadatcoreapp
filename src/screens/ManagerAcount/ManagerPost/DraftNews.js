import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {Text} from 'react-native-paper';
import {styles} from '../../../core/Acount/draftnews/styles';
import Selection from '../../../components/Selection';
import Const from '../../../components/Const';
import ModalProcess from '../../../components/ListModal/ModalProcess';

export default function DraftNews({navigation}) {
  const [modalProcess, setModalProcess] = useState(false);
  const setCloseModal = item => {
    setModalProcess(false);
  };
  const [listData, setListData] = useState([
    {status: 1},
    {status: 2},
    {status: 3},
    {status: 1},
    {status: 2},
  ]);

  const renderItem = ({item}) => (
    <View style={styles.blockItem}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('DetailPost');
        }}
        style={styles.blockImageInfo}>
        <Image
          source={require('../../../assets/dashboard/Group2391.png')}
          style={styles.imageItem}
        />
        <View style={styles.rightItem}>
          <View style={styles.titleImage}>
            <Text style={styles.titleRightItem}>
              Tổng hợp các căn hộ cần chuyển nhượng giá gốc Vinhomes Ocean Park,
              liên...
            </Text>
            <Image
              source={require('../../../assets/Vector.png')}
              style={styles.imageRightTitle}
            />
          </View>
          <Text style={styles.PriceInfo}>1.65 tỷ · 75 m² · 2 PN · 2 WC</Text>
        </View>
      </TouchableOpacity>
      <View style={styles.blockTimeTrash}>
        <Text style={styles.timeRightItem}>
          Cập nhật lần cuối: 31/07/2021 14:35
        </Text>
        <TouchableOpacity
          onPress={() => {
            setModalProcess(true);
          }}
          style={styles.blockTrash}>
          <Image
            source={require('../../../assets/Acount/draftNews/trash.png')}
            style={{width: 20, height: 20}}
          />
          <Text style={styles.textTrash}>Xóa</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <ModalProcess
        visible={modalProcess}
        title={'Xác nhận xoá tin nháp'}
        content={'Bạn có chắc chắn muốn xoá tin nháp này hay không?'}
        typeButton={2}
        onChange={item => setCloseModal(item)}
      />
      <Selection size={16}>
        <Text style={styles.textTotal}>Có 1 tin nháp</Text>
      </Selection>
      <FlatList data={listData} renderItem={renderItem} />
    </SafeAreaView>
  );
}
