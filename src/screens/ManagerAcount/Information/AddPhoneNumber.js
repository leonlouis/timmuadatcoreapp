import React, {useState} from 'react';
import {SafeAreaView, View} from 'react-native';
import {Text} from 'react-native-paper';
import Button from '../../../components/Button';
import {styles} from '../../../core/Acount/addphonenumber/styles';
import Header from '../../../components/Header';
import Selection from '../../../components/Selection';
import {Container} from 'native-base';
import TextInput from '../../../components/TextInput';
import OtpInputs from 'react-native-otp-inputs';
import ModalProcess from '../../../components/ListModal/ModalProcess';

export default function AddPhoneNumber({navigation}) {
  const [otp, setOtp] = useState('');
  const [modalProcess, setModalProcess] = useState(false);
  const setCloseModal = item => {
    setModalProcess(false);
  };
  return (
    <Container style={{flex: 1}}>
      <Header keyHeader="Thêm số điện thoại" navigation={navigation} />
      <SafeAreaView style={styles.container}>
        <ModalProcess
          visible={modalProcess}
          title={'Thêm số điện thoại đăng tin thành công!'}
          content={'Thêm số điện thoại đăng tin thành công!'}
          typeButton={1}
          onChange={item => setCloseModal(item)}
        />
        <Selection size={16}>
          <View>
            <TextInput
              style={styles.inputLogin}
              placeholder="Số điện thoại đăng tin*"
              placeholderTextColor="#999999"
              returnKeyType="next"
              // value={fullname.value}
              // onChangeText={text => setFullname({value: text, error: ''})}
              // error={!!fullname.error}
              // errorText={fullname.error}
              theme={{colors: {primary: '#999999', text: '#000000'}}}
              autoCapitalize="none"
              marginVerNone={0}
            />
          </View>
          <Button
            labelStyle={styles.buttonTitle}
            style={styles.buttonLogin}
            type={'blue'}
            mode="contained"
            onPress={() => {
              setModalProcess(true);
            }}>
            Lấy mã xác thực
          </Button>
          <View style={styles.blockCdtime}>
            <Text style={styles.textCoundown}>
              Nhập mã số xác thực đã được gửi SMS đến số điện thoại của bạn.
            </Text>
            <Text style={styles.textCoundown}>Thời gian còn lại:</Text>
          </View>
          <Text style={styles.timeCdown}>15:00</Text>
          <View style={styles.blockOTP}>
            <OtpInputs
              inputContainerStyles={styles.containerStyle}
              inputStyles={styles.inputStyle}
              handleChange={code => setOtp(code)}
              numberOfInputs={6}
            />
            <Text style={styles.textWarning}>
              Mã số không chính xác vui lòng thử lại!
            </Text>
          </View>
        </Selection>
      </SafeAreaView>
    </Container>
  );
}
