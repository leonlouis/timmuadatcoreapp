import React from 'react';
import { View } from 'react-native';

function Flex({ children, style, row, flex, ...props }) {
    return (
        <View
            style={[
                {
                    flexDirection: row ? 'row' : 'column',
                    flex: flex ? 1 : 0,
                },
                style,
            ]}
            {...props}
        >
            {children}
        </View>
    );
}

Flex.defaultProps = {
    row: false,
};

export default Flex;
