import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {Text} from 'react-native-paper';
import {styles} from '../../../core/Acount/retalnews/styles';
import {Container} from 'native-base';
import Selection from '../../../components/Selection';
import Swipeable from 'react-native-swipeable';
import TextInput from '../../../components/TextInput';
import Const from '../../../components/Const';

export default function RentalNews({navigation}) {
  const [listData, setListData] = useState([
    {status: 1},
    {status: 2},
    {status: 3},
    {status: 1},
    {status: 2},
  ]);
  const textStatus = item => {
    if (item === 2) {
      return <Text style={{color: Const.GREEN}}>Đã duyệt</Text>;
    } else if (item === 3) {
      return <Text style={{color: Const.RED_v2}}>Không duyệt</Text>;
    } else {
      return <Text>Chưa duyệt</Text>;
    }
  };
  const rightButtons = [
    <TouchableOpacity style={styles.StatusSwipeableLeft}>
      <Image
        source={require('../../../assets/Acount/rentalNews/bxs-edit.png')}
      />
      <Text style={styles.textStatus}>Sửa</Text>
    </TouchableOpacity>,
    <TouchableOpacity style={styles.StatusSwipeableRight}>
      <Image
        source={require('../../../assets/Acount/rentalNews/bx-trash.png')}
      />
      <Text style={styles.textStatus}>Xóa</Text>
    </TouchableOpacity>,
  ];
  const renderItem = ({item}) => (
    <Swipeable rightButtons={rightButtons}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('DetailPost');
        }}
        style={styles.blockItem}>
        <Image
          source={require('../../../assets/dashboard/Group2391.png')}
          style={styles.imageItem}
        />
        <View style={styles.rightItem}>
          <View style={styles.titleImage}>
            <Text style={styles.titleRightItem}>
              Tổng hợp các căn hộ cần chuyển nhượng giá gốc Vinhomes Ocean Park,
              liên...
            </Text>
            <Image
              source={require('../../../assets/Vector.png')}
              style={styles.imageRightTitle}
            />
          </View>
          <Text style={styles.PriceInfo}>1.65 tỷ · 75 m² · 2 PN · 2 WC</Text>
          <Text style={styles.timeRightItem}>
            06/10/2021 · Tin thường · {textStatus(item.status)}
          </Text>
        </View>
      </TouchableOpacity>
    </Swipeable>
  );

  return (
    <SafeAreaView style={styles.container}>
      <Selection size={16}>
        <View style={styles.totalNews}>
          <Text style={styles.textTotal}>Có 5 tin rao tìm thấy</Text>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('FilterPost');
            }}
            style={styles.iconFind}>
            <Image
              source={require('../../../assets/dashboard/Group1755.png')}
            />
          </TouchableOpacity>
        </View>
      </Selection>
      <FlatList data={listData} renderItem={renderItem} />
    </SafeAreaView>
  );
}
