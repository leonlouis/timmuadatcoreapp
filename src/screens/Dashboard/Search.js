import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  View,
  FlatList,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {styles} from '../../core/dashboard/Search/styles';
import {Container} from 'native-base';
import TextInput from '../../components/TextInput';
import Selection from '../../components/Selection';

const {width, height} = Dimensions.get('screen');
import {useFocusEffect} from '@react-navigation/native';
import MapView from 'react-native-maps';
import {Text} from 'react-native-paper';
import {DashboardModel} from '../../models/dashboard';

export default function Search({route, navigation}, ...props) {
  const {dataFilter} = route?.params;
  const [keyQ, setKeyQ] = useState({value: dataFilter?.kSearch || ''});
  const [listData, setListData] = useState([]);
  const [listFilter, setListFilter] = useState([]);
  useFocusEffect(
    React.useCallback(() => {
      let ArrayFilter = [];
      if (dataFilter?.adverOption && dataFilter?.adverOption?.value !== '') {
        ArrayFilter.push({
          name: dataFilter?.adverOption?.name,
          value: dataFilter?.adverOption?.value,
        });
      }
      if (
        dataFilter?.categoriesOption &&
        dataFilter?.categoriesOption?.id !== ''
      ) {
        ArrayFilter.push({
          name: dataFilter?.categoriesOption?.name,
          value: dataFilter?.categoriesOption?.id,
        });
      }
      if (dataFilter?.cityOption && dataFilter?.cityOption?.id !== '') {
        ArrayFilter.push({
          name: dataFilter?.cityOption?.name,
          value: dataFilter?.cityOption?.id,
        });
      }
      if (dataFilter?.PriceOption && dataFilter?.PriceOption !== '') {
        ArrayFilter.push({
          name: dataFilter?.PriceOption,
          value: dataFilter?.PriceOption,
        });
      }
      if (dataFilter?.areaOption && dataFilter?.areaOption !== '') {
        ArrayFilter.push({
          name: dataFilter?.areaOption,
          value: dataFilter?.areaOption,
        });
      }
      if (dataFilter?.projectOption && dataFilter?.projectOption.id !== '') {
        ArrayFilter.push({
          name: dataFilter?.projectOption?.name,
          value: dataFilter?.projectOption?.id,
        });
      }
      if (
        dataFilter?.districtsOption &&
        dataFilter?.districtsOption?.id !== ''
      ) {
        ArrayFilter.push({
          name: dataFilter?.districtsOption?.name,
          value: dataFilter?.districtsOption?.id,
        });
      }
      if (dataFilter?.wardsOption && dataFilter?.wardsOption?.id !== '') {
        ArrayFilter.push({
          name: dataFilter?.wardsOption?.name,
          value: dataFilter?.wardsOption?.id,
        });
      }
      if (dataFilter?.roomOption && dataFilter?.roomOption?.value !== '') {
        ArrayFilter.push({
          name: dataFilter?.roomOption?.name,
          value: dataFilter?.roomOption?.value,
        });
      }
      if (
        dataFilter?.directionOption &&
        dataFilter?.directionOption?.value !== ''
      ) {
        ArrayFilter.push({
          name: dataFilter?.directionOption?.name,
          value: dataFilter?.directionOption?.value,
        });
      }
      setListFilter(ArrayFilter);
      setKeyQ({value: dataFilter?.kSearch || ''});
      getDataInterestProducts();
    }, [dataFilter]),
  );
  const getDataInterestProducts = () => {
    DashboardModel.getListInterestProducts(
      dataFilter?.cityOption?.id,
      dataFilter?.districtsOption?.id,
      dataFilter?.wardsOption?.id,
      dataFilter?.directionOption?.value,
      dataFilter?.roomOption?.value,
      dataFilter?.adverOption?.value,
      dataFilter?.categoriesOption?.id,
      dataFilter?.PriceOption,
      dataFilter?.areaOption,
      dataFilter?.projectOption?.id,
      keyQ.value,
      '',
      '',
    )
      .then(data => {
        if (data.status == 1) {
          setListData(data.data.listProducts);
        }
      })
      .catch(e => {
        navigation.navigate('Dashboard', {object: {}});
      });
  };
  const renderItem = ({item}) => (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('DetailPostDashboard');
      }}
      style={styles.blockImage_header}>
      <View style={styles.blockItemNew}>
        {item?.is_hot == 0 ? (
          <Image
            source={require('../../assets/dashboard/Path616.png')}
            style={styles.star5}
          />
        ) : (
          <View />
        )}
        <Text style={styles.titleNew}>
          {item?.is_hot == 0 ? '      ' : null}
          {item?.title} LH: {item?.seller_phone_number}
        </Text>
        <View style={styles.blockItemW}>
          <View>
            {item?.avatar ? (
              <Image
                source={{
                  uri: item?.avatar,
                }}
                style={styles.wblock}
              />
            ) : (
              <Image
                source={require('../../assets/dashboard/no_image.png')}
                style={styles.wblock}
              />
            )}
            <View style={styles.totalImage}>
              <Text style={styles.totalIm}>{item?.total_images || 0}</Text>
              <Image source={require('../../assets/dashboard/bx-image.png')} />
            </View>
          </View>
          <View>
            <Text style={styles.price_m_w}>
              {item?.price} · {item?.area || 0} m² ·{' '}
              {item?.bedroom_numbers || 0} PN · {item?.toilet_numbers || 0} WC
            </Text>
            <Text style={[styles.price_m, {marginBottom: 12}]}>
              {item?.address}
            </Text>
            <Text style={styles.time_m}>{item?.start_date}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
  const renderItemFilter = ({item}) => (
    <TouchableOpacity style={styles.textFindata}>
      <Text style={styles.itemTextFinData}>{item?.name}</Text>
    </TouchableOpacity>
  );
  return (
    <Container style={{flex: 1}}>
      <SafeAreaView style={styles.container}>
        <Selection size={16}>
          <View style={styles.blockSearch}>
            <View>
              <TouchableOpacity
                onPress={() => {
                  getDataInterestProducts();
                }}
                style={styles.iconInptuSearch}>
                <Image
                  source={require('../../assets/dashboard/SearchGlyphLight.png')}
                />
              </TouchableOpacity>
              <TextInput
                style={[
                  styles.inputLogin,
                  {
                    textAlign: 'left',
                    width: width - 86,
                  },
                ]}
                underlineColor="transparent"
                placeholder="Nhập từ khóa tìm kiếm..."
                placeholderTextColor="#999999"
                returnKeyType="next"
                value={keyQ.value}
                onChangeText={text => setKeyQ({value: text})}
                theme={{colors: {primary: '#999999', text: '#000000'}}}
                autoCapitalize="none"
                autoCompleteType="email"
                textContentType="emailAddress"
                keyboardType="email-address"
                marginVerNone={0}
              />
            </View>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('Dashboard', {
                  object: {keySearch: keyQ.value, filterList: dataFilter},
                })
              }
              style={styles.iconFind}>
              <Text style={styles.textBack}>Hủy</Text>
            </TouchableOpacity>
          </View>
        </Selection>
        <View style={styles.blockFilterSearch}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('FilterPostDashboard', {
                object: {keySearch: keyQ.value, filterList: dataFilter},
              });
            }}
            style={styles.iconFindData}>
            <Image source={require('../../assets/dashboard/Group1755.png')} />
          </TouchableOpacity>
          <FlatList
            horizontal={true}
            data={listFilter}
            renderItem={renderItemFilter}
          />
        </View>
        <ScrollView>
          {/*<MapView*/}
          {/*  style={styles.ggMap}*/}
          {/*  initialRegion={{*/}
          {/*    latitude: 21.0277644,*/}
          {/*    longitude: 105.8341598,*/}
          {/*    latitudeDelta: 0.0922,*/}
          {/*    longitudeDelta: 0.0421,*/}
          {/*  }}*/}
          {/*  showsUserLocation*/}
          {/*  minZoomLevel={11}*/}
          {/*/>*/}
          <Selection size={16}>
            {listData.length > 0 ? (
              <View>
                {/*<View style={styles.borderBottom} />*/}
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('DetailPostDashboard');
                  }}
                  style={styles.blockImage_header}>
                  <View>
                    {listData[0]?.avatar ? (
                      <Image
                        source={{
                          uri: listData[0]?.avatar,
                        }}
                        style={styles.w100}
                      />
                    ) : (
                      <Image
                        source={require('../../assets/dashboard/no_image.png')}
                        style={styles.w100}
                      />
                    )}
                    <View style={styles.hotnew}>
                      <Text style={styles.vluenew}>Nổi bật</Text>
                    </View>
                    <View style={styles.totalImage}>
                      <Text style={styles.totalIm}>
                        {listData[0]?.total_images || 0}
                      </Text>
                      <Image
                        source={require('../../assets/dashboard/bx-image.png')}
                      />
                    </View>
                    <View style={styles.totalImage_phone}>
                      <Image
                        source={require('../../assets/dashboard/bx-phone-call.png')}
                      />
                      <Text style={styles.totalIm_phone}>
                        {listData[0]?.seller_phone_number}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.blockItemNew}>
                    <Image
                      source={require('../../assets/dashboard/Path616.png')}
                      style={styles.star5}
                    />
                    <Text style={styles.titleNew}>
                      {'      '}
                      {listData[0]?.title}
                    </Text>
                    <View style={styles.price_time}>
                      <Text style={styles.price_m}>
                        {listData[0]?.price} · {listData[0]?.area || 0} m²
                      </Text>
                      <Text style={styles.time_m}>
                        {listData[0]?.start_date}
                      </Text>
                    </View>
                    <Text style={styles.price_m}>
                      {listData[0]?.seller_address}
                    </Text>
                  </View>
                </TouchableOpacity>
                <FlatList data={listData.slice(1)} renderItem={renderItem} />
              </View>
            ) : (
              <View />
            )}
          </Selection>
        </ScrollView>
      </SafeAreaView>
    </Container>
  );
}
