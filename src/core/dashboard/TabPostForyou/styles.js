import {DefaultTheme} from 'react-native-paper';
import {StatusBar} from 'react-native';
import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  w100: {
    width: '100%',
    height: 192,
    borderRadius: 8,
  },
  totalImage: {
    flexDirection: 'row',
    padding: 6,
    backgroundColor: '#000000',
    borderRadius: 6,
    position: 'absolute',
    top: 6,
    right: 6,
    zIndex: 9999,
  },
  totalImage_phone: {
    flexDirection: 'row',
    padding: 6,
    backgroundColor: '#429134',
    borderRadius: 6,
    position: 'absolute',
    bottom: 6,
    right: 6,
    zIndex: 9999,
  },
  totalIm: {
    ...SanFrancisco.regular,
    fontSize: 13,
    lineHeight: 15,
    color: Const.WHITE,
    marginRight: 6,
  },
  totalIm_phone: {
    ...SanFrancisco.regular,
    fontSize: 13,
    lineHeight: 15,
    color: Const.WHITE,
    marginLeft: 6,
  },
  hotnew: {
    paddingTop: 5,
    paddingBottom: 4,
    paddingLeft: 6,
    paddingRight: 7,
    backgroundColor: '#CF2E33',
    borderRadius: 4,
    position: 'absolute',
    top: 6,
    left: 6,
    zIndex: 9999,
  },
  vluenew: {
    ...SanFrancisco.medium,
    fontSize: 13,
    lineHeight: 15,
    color: Const.WHITE,
  },
  blockImage_header: {
    // marginBottom: 16,
    borderWidth: 1,
    borderColor: '#E6E6E6',
    borderRadius: 8,
    marginBottom: 16,
  },
  star5: {
    position: 'absolute',
    top: 16,
    left: 16,
  },
  titleNew: {
    fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 20,
    color: Const.BLACK,
  },
  blockItemNew: {
    padding: 16,
  },
  price_time: {
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 12,
  },
  price_m: {
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 20,
    color: Const.BLACK,
  },
  time_m: {
    fontWeight: 'bold',
    fontSize: 13,
    lineHeight: 15,
    color: Const.GRAY_v4,
  },
  wblock: {
    width: 148,
    height: 93,
    borderRadius: 8,
  },
  blockItemW: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 16,
  },
  price_m_w: {
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 21,
    color: Const.BLACK,
    marginBottom: 12,
  },
};
