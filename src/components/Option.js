import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Flex from './Flex';
import SanFrancisco from './SanFrancisco';
import Const from './Const';

function Option({value, checked, label, style, onChange}) {
  return (
    <TouchableOpacity
      onPress={() => {
        onChange(value);
      }}
      style={style}>
      <Flex
        row
        style={{
          alignItems: 'center',
        }}>
        <MaterialCommunityIcons
          name={checked === value ? 'radiobox-marked' : 'radiobox-blank'}
          size={24}
          color={checked === value ? '#004182' : '#C7C7CC'}
        />
        <Text> </Text>
        <Text style={styles.labelTitle}>{label}</Text>
      </Flex>
    </TouchableOpacity>
  );
}

Option.defaultProps = {
  onChange: () => {},
  style: {},
};
export const styles = {
  labelTitle: {
    ...SanFrancisco.regular,
    fontSize: 17,
    lineHeight: 22,
    color: Const.BLACK,
    marginLeft: 15,
  },
};

export default Option;
