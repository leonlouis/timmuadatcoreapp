import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  tabBar: {
    flexDirection: 'row',
    marginTop: 18,
    backgroundColor: '#7676801F',
    marginLeft: 16,
    marginRight: 16,
    marginBottom: 20,
    borderRadius: 7,
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    borderRadius: 7,
  },
  titleTab: {
    paddingLeft: 14,
    paddingRight: 14,
    marginTop: 8,
    marginBottom: 8,
    ...SanFrancisco.semiBold,
    fontSize: 13,
    lineHeight: 21,
    color: '#ffffff',
  },
};
