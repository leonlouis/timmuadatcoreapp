import SanFrancisco from '../../../components/SanFrancisco';
import Const from '../../../components/Const';

export const styles = {
  tabContent: {flex: 1},
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  buttonTitle: {
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 22,
    color: Const.WHITE,
  },
  buttonLogin: {
    borderRadius: 8,
    height: 50,
    borderWidth: 1,
    borderColor: Const.BLUE,
    // marginTop: 13,
    marginBottom: 16,
    marginTop: 16,
  },
  blockCdtime: {
    marginBottom: 16,
  },
  textCoundown: {
    ...SanFrancisco.regular,
    fontSize: 16,
    lineHeight: 24,
    color: Const.BLACK,
  },
  timeCdown: {
    textAlign: 'center',
    ...SanFrancisco.semiBold,
    fontSize: 17,
    lineHeight: 20,
    color: Const.BLACK_ROOT,
  },
  containerStyle: {
    backgroundColor: Const.GRAY_v6,
    width: 57,
    height: 47,
    borderRadius: 8,
  },
  inputStyle: {
    color: Const.BLACK_ROOT,
    ...SanFrancisco.regular,
    textAlign: 'center',
    fontSize: 17,
    lineHeight: 22,
  },
  blockOTP: {
    marginTop: 34,
  },
  textWarning: {
    textAlign: 'center',
    marginTop: 38,
    ...SanFrancisco.regular,
    fontSize: 14,
    lineHeight: 16,
    color: Const.RED,
  },
  inputLogin: {
    height: 60,
    fontSize: 17,
    lineHeight: 20,
    ...SanFrancisco.regular,
    color: '#000000',
    textAlign: 'left',
  },
};
